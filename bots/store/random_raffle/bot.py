"""
python bot.py \
    --bot_loop_time 5 \
    --verbose
"""
import time
from datetime import datetime,timezone
import random


from config import *

def check():
    # retrieve from STORE_COLLECTION those items with end_date < now and status == true
    # and type is 'random_raffe'
    now_utc = datetime.now(timezone.utc)
    open_items = list(STORE_COLLECTION.find(
        {"type": "random_raffle", "status": True, "end_date": {"$lt": now_utc}}
    ))

    if args.verbose:
        print(f"\nThere are {len(open_items)} items to be raffled")
    
    for item in open_items:
        if args.verbose:
            print(f"Raffling {item['_id']}")

        winners = []
        if item["participations"] and item["prizes"]:
            entries_names = [entry["user_id"] for entry in item["participations"]]
            entries_tickets = [entry["participations"] for entry in item["participations"]]

            for _ in range(item["prizes"]):

                if not sum(entries_tickets):
                    if args.verbose:
                        print("No more tickets")
                    break

                # First we get a random index from the list of entries based on the number of tickets
                entries_indices = [i for i in range(len(entries_tickets))]
                winner_index = random.choices(entries_indices, weights=entries_tickets, k=1)[0]

                # Then we get the name of the winner
                winner_name = entries_names[winner_index]
                winners.append(winner_name)

                if item["multi_win"]: # Finally we subtract one ticket from the winner entries_tickets
                    if entries_tickets[winner_index] <= 1: # If we subtract 1, the user will have 0 tickets
                        del entries_names[winner_index]
                        del entries_tickets[winner_index]
                    else: # User has more than 1 ticket, subtract if can have more than one prize
                        entries_tickets[winner_index] -= 1
                else: # If not multi_win, remove the user from eligible options
                    del entries_names[winner_index]
                    del entries_tickets[winner_index]
    

        # Update the item with the winners and status false
        STORE_COLLECTION.update_one(
            {"_id": item["_id"]},
            {"$set": {"status": False, "winners": winners}}
        )

        # Add to each winner the prize
        prize = {
            "type": item["prize_type"],
            "status": "pending",
            "game": "random_raffle",
        }
        for winner in winners:
            USERS_COLLECTION.update_one(
                {"_id": winner},
                {"$push": {"prizes": prize}}
            )

        if args.verbose:
            print(f"{item['_id']} raffled")


print("==== Bot started ====")
# Print the arguments
print(f"Arguments: {args}\n")

while True:
    try:
        check()
    except Exception as e:
        print(f"[ERROR]: {e}")
        pass

    time.sleep(int(args.bot_loop_time))


