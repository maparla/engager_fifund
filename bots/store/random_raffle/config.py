"""
    This file contains the configuration of the arguments and database for the bot
"""
import argparse

import pymongo


""" Arguments """
_args = argparse.ArgumentParser()

_args.add_argument(
    "--bot_loop_time",
    required=False, 
    default=5,
    help="Collection from which watch sales and verify at Discord"
)

_args.add_argument(
    "--verbose", 
    action="store_true",
    required=False, default=False, 
    help="Show verbose results (default: %(default)s)"
)
args = _args.parse_args()


""" Database """
DB = "Games"
MONGO_USER = "maparla"
MONGO_PASSWORD = "RI9ARUsN27PjSH25"
MONGO_CLUSTER = "cluster0.fbotufa.mongodb.net"

MONGO_CLIENT = pymongo.MongoClient(f"mongodb+srv://{MONGO_USER}:{MONGO_PASSWORD}@{MONGO_CLUSTER}")

MONGO_DB = MONGO_CLIENT[DB]
STORE_COLLECTION = MONGO_DB["store"]
USERS_COLLECTION = MONGO_DB["users"]

