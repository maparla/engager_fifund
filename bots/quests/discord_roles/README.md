# RarityBot


## Description

The idea of this bot is to:

1. Given an Account of a collection, watch for bundle sales and store them on the centralized database.

**Important:** We need to put the bot above the user roles we want to manage!

## Prerequisites: Installations

For testing purposes the best is to create a virtual environment and install the dependencies. You can follow this [guide](https://www.digitalocean.com/community/tutorials/how-to-install-python-3-and-set-up-a-programming-environment-on-ubuntu-20-04-quickstart-es).

We can create docker container for a quest `role_dicord_test` with the following command:

```bash
docker build -t fifund_quests_by_role:latest .

docker run -d \
    --name role_dicord_test \
    -e BOT_KEY=OTkyMDY4ODkzMTg1MTQ2OTYw.GrlMVq.tR1xihjk5exkicTwZdn3ZVn3b-R9ExlblDeeDU \
    -e DISCORD_GUILD=1021418616647856148 \
    -e ROLE_ID=1028770109055057950 \
    -e QUEST_ID=role_dicord_test \
    -e CREDITS=3 \
    -e BOT_LOOP_TIME=60 \
    -e PYTHONUNBUFFERED=1 \
    fifund_quests_by_role
```