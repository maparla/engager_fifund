"""
    This file contains the configuration of the arguments and database for the bot
"""
import argparse

from motor.motor_asyncio import AsyncIOMotorClient

from utils import *

"""
    Arguments
"""
_args = argparse.ArgumentParser()
_args.add_argument(
    "--bot_key",
    required=True, 
    help="The Discord bot key"
)
_args.add_argument(
    "--discord_guild",
    required=True, 
    help="The Discord guild id to check"
)
_args.add_argument(
    "--role_id",
    required=True, 
    help="The Discord role id to check"
)

_args.add_argument(
    "--quest_id",
    required=True, 
    help="The id of the quest that envolves the bot"
)

_args.add_argument(
    "--credits",
    required=True, 
    help="The amount of credits to give to the user"
)

_args.add_argument(
    "--bot_loop_time",
    required=False, 
    default=5,
    help="Collection from which watch sales and verify at Discord"
)

_args.add_argument(
    "--verbose", 
    action="store_true",
    required=False, default=False, 
    help="Show verbose results (default: %(default)s)"
)
args = _args.parse_args()


"""
    Database
"""
DB = "Games"
MONGO_USER = "maparla"
MONGO_PASSWORD = "RI9ARUsN27PjSH25"
MONGO_CLUSTER = "cluster0.fbotufa.mongodb.net"

MONGO_CLIENT = AsyncIOMotorClient(f"mongodb+srv://{MONGO_USER}:{MONGO_PASSWORD}@{MONGO_CLUSTER}")

MONGO_DB = MONGO_CLIENT[DB]
USERS_COLLECTION = MONGO_DB["users"]

BOT_LOOP_TIME = int(args.bot_loop_time)
ROLE_ID = int(args.role_id)
QUEST_ID = args.quest_id
CREDITS = int(args.credits)
DISCORD_GUILD = int(args.discord_guild)