"""
python bot.py \
    --bot_key=OTkyMDY4ODkzMTg1MTQ2OTYw.GrlMVq.tR1xihjk5exkicTwZdn3ZVn3b-R9ExlblDeeDU \
    --discord_guild=1021418616647856148 \
    --role_id=1028770109055057950 \
    --quest_id=screenshot_raid_twitter_qt1 \
    --credits=3 \
    --bot_loop_time=10 \
    --verbose
"""
# https://github.com/DisnakeDev/disnake/blob/master/examples/background_task.py
import time
import datetime
import platform
import traceback

import disnake
from disnake.ext import tasks

from config import *


class MyClient(disnake.Client):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        # start the task to run in the background
        self.my_background_task.start()

    async def on_ready(self):
        """
        The code in this even is executed when the bot is ready
        """
        print(f"Logged in as {self.user} (ID: {self.user.id})")
        print(f"disnake API version: {disnake.__version__}")
        print(f"Bot loop time: {BOT_LOOP_TIME}")
        print(f"Checking role: {ROLE_ID}")
        print(f"Quest id: {QUEST_ID}")
        print(f"Credits: {CREDITS}")
        print(f"Python version: {platform.python_version()}")
        print("-------------------")

    # task runs every x time, if the task is not running it will start it and wait the specified time
    # if the task is running it will not start it and wait till finished to start it again without delay
    @tasks.loop(seconds=BOT_LOOP_TIME)
    async def my_background_task(self):

        start_time = time.time()

        try: 

            discord_guild = self.get_guild(DISCORD_GUILD)
            if not discord_guild:
                print("[ERROR] Discord guild not found")
                assert False, "Discord guild not found"

            server_members = discord_guild.members
            server_roled_members = [member for member in server_members if ROLE_ID in [role.id for role in member.roles]]

            # Get the users where completed_quests (list) has not the quest args.quest_id
            users = await USERS_COLLECTION.aggregate([
                {
                    '$match': {
                        'completed_quests': {
                            '$nin': [
                                QUEST_ID
                            ]
                        }
                    }
                }, {
                    '$lookup': {
                        'from': 'accounts', 
                        'localField': '_id', 
                        'foreignField': 'userId', 
                        'as': 'account_info'
                    }
                }
            ]).to_list(length=None)

            print(f"Detected server members: {len(server_members)}")
            print(f"Detected server members with role: {len(server_roled_members)}")
            print(f"Detected database users without quest: {len(users)}")

            for user_roled in server_roled_members:

                # Get the user from the database
                user = [user for user in users if user["account_info"][0]["providerAccountId"] == str(user_roled.id)]
                if not user:
                    continue

                user = user[0]
                # Push the quest_id to the completed_quests list and increment the user credits
                await USERS_COLLECTION.update_one(
                    {"_id": user["_id"]},
                    {
                        "$push": {"completed_quests": QUEST_ID}, 
                        "$inc": {"credits": int(CREDITS)}
                    }
                )
    
        except Exception as e:
            print(f"[ERROR] {e}")
            traceback.print_exc()


        elapsed_seconds = time.time() - start_time
        total_time = str(datetime.timedelta(seconds=elapsed_seconds))
        print(f"Processed in {total_time}\n")

    @my_background_task.before_loop
    async def before_my_task(self):
        await self.wait_until_ready()  # wait until the bot logs in


# https://docs.disnake.dev/en/latest/intents.html
intents = disnake.Intents.default()
intents.typing = False
intents.presences = False
intents.members = True

client = MyClient(intents=intents)
client.run(args.bot_key)
