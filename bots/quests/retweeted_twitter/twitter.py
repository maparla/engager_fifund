import tweepy

# assign the values accordingly
BEARER_TOKEN = "AAAAAAAAAAAAAAAAAAAAAAvyfgEAAAAA6wnsnz8Nz9zvne1iSfU1o%2Bq6QUk%3DlUyPunH3kNZkbFxVj9Qt8OV6vZmlkrWUOGiazk7Oo5KnbTiZ7R"

# Authenticate to Twitter
TWITTER_CLIENT = tweepy.Client(BEARER_TOKEN, wait_on_rate_limit=True)

try:
    TWITTER_CLIENT.verify_credentials()
    print("Twitter Authentication OK")
except:
    print("Error during authentication")


def get_retweeters(tweet_id):
    next_token = None
    retweeters = []

    while True:
        # By default, only the ID, name, and username fields of each user will be returned
        response = TWITTER_CLIENT.get_retweeters(
            id=tweet_id, pagination_token=next_token
        )

        if response.meta['result_count'] > 0:
            for user in response.data:
                retweeters.append(user.id)
        else: 
            break

        if 'next_token' in response.meta:
            next_token = response.meta['next_token']
        else:
            break

    return retweeters