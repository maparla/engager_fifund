"""
python bot.py \
    --tweet_id 1434415512264613888 \
    --quest_id 1434415512264613888_retweet \
    --credits 2 \
    --bot_loop_time 200 \
    --verbose
"""
import time
from twitter import get_retweeters


from config import *

def check():

    start = time.time()

    # Get the users with socials.twitter.user_id linked
    # and completed_quests (list) has not the quest args.quest_id
    users = list(USERS_COLLECTION.find(
        {
            "socials.twitter.user_id": {'$exists': True},
            "completed_quests": {'$nin': [args.quest_id]}
        }
    ))

    if not users:
        print("No users to check")
        return time.time() - start

    retweeters = get_retweeters(args.tweet_id)
    retweeters = [str(retweeter) for retweeter in retweeters]

    if args.verbose:
        print(f"Tweet {args.tweet_id} has {len(retweeters)} retweeters")

    for user in users:
        if str(user["socials"]["twitter"]["user_id"]) in retweeters:
            
            if args.verbose:
                print(f"{user['name']} retweeted {args.tweet_id}")

            # Push the quest_id to the completed_quests list and increment the user credits
            USERS_COLLECTION.update_one(
                {"_id": user["_id"]},
                {
                    "$push": {"completed_quests": args.quest_id}, 
                    "$inc": {"credits": int(args.credits)}
                }
            )

        else:
            if args.verbose:
                print(f"{user['name']} not retweeted {args.tweet_id}")

    return time.time() - start

print("==== Bot started ====")
# Print the arguments
print(f"Arguments: {args}\n")

while True:
    try:
        elapsed_time = check()
        if elapsed_time < int(args.bot_loop_time):
            if args.verbose:
                print(f"Sleeping {int(int(args.bot_loop_time) - elapsed_time)} seconds")
            time.sleep(int(args.bot_loop_time) - elapsed_time)  
    except Exception as e:
        print(f"[ERROR]: {e}")
        pass
