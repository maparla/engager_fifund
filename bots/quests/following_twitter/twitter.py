import tweepy

# assign the values accordingly
CONSUMER_KEY = "Jz7lkOhgrbs6Mhre8BX008ula"
CONSUMER_SECRET = "mHusXri4GtcSKJs0iCzSh3qfFOdl7Y81zq3DI9MMHtBHBLCcSE"
ACCESS_TOKEN = "1524651454195249153-noX5kYc8Qcvug0gFvIGRF7cRSxIsV2"
ACCESS_TOKEN_SECRET = "TUhwziFI4GCUMcVpQ9DNROJ446i7zM0UFN9zDtVHbk89c"

# Authenticate to Twitter
auth = tweepy.OAuthHandler(
    CONSUMER_KEY,
    CONSUMER_SECRET
)

auth.set_access_token(
    ACCESS_TOKEN,
    ACCESS_TOKEN_SECRET
)

TWITTER_API = tweepy.API(auth, wait_on_rate_limit=True)

try:
    TWITTER_API.verify_credentials()
    print("Twitter Authentication OK")
except:
    print("Error during authentication")


def get_follower_ids(screen_name, count=5000):
    # get the list of all followers of a user
    # return a list of ids
    followerids = []

    for user in tweepy.Cursor(
        TWITTER_API.get_follower_ids,
        screen_name=screen_name,
        count=count
    ).items():
        followerids.append(user)

    return followerids
