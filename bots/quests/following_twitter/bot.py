"""
python bot.py \
    --twitter_account FiFund_io \
    --quest_id fifund_twitter \
    --credits 3 \
    --bot_loop_time 300 \
    --verbose
"""
import time
from twitter import get_follower_ids


from config import *

def check():

    start = time.time()

    # Get the users with socials.twitter.user_id linked
    # and completed_quests (list) has not the quest args.quest_id
    users = list(USERS_COLLECTION.find(
        {
            "socials.twitter.user_id": {'$exists': True},
            "completed_quests": {'$nin': [args.quest_id]}
        }
    ))

    if not users:
        print("No users to check")
        return time.time() - start

    followers = get_follower_ids(args.twitter_account)
    followers = [str(follower) for follower in followers]

    if args.verbose:
        print(f"{args.twitter_account} has {len(followers)} followers")

    for user in users:
        if str(user["socials"]["twitter"]["user_id"]) in followers:
            
            if args.verbose:
                print(f"{user['name']} is following {args.twitter_account}")

            # Push the quest_id to the completed_quests list and increment the user credits
            USERS_COLLECTION.update_one(
                {"_id": user["_id"]},
                {
                    "$push": {"completed_quests": args.quest_id}, 
                    "$inc": {"credits": int(args.credits)}
                }
            )

        else:
            if args.verbose:
                print(f"{user['name']} is not following {args.twitter_account}")

    return time.time() - start

print("==== Bot started ====")
# Print the arguments
print(f"Arguments: {args}\n")

while True:
    try:
        elapsed_time = check()
        if elapsed_time < int(args.bot_loop_time):
            if args.verbose:
                print(f"Sleeping {int(int(args.bot_loop_time) - elapsed_time)} seconds")
            time.sleep(int(args.bot_loop_time) - elapsed_time)  
    except Exception as e:
        print(f"[ERROR]: {e}")
        pass
