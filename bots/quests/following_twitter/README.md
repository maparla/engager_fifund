# Random Raffle Bot

## Description

The idea of this bot is to raffle the random raffles from fifund engager platform.

## Prerequisites: Installations

For testing purposes the best is to create a virtual environment and install the dependencies. You can follow this [guide](https://www.digitalocean.com/community/tutorials/how-to-install-python-3-and-set-up-a-programming-environment-on-ubuntu-20-04-quickstart-es).

We need to install [metaplex decoder](https://github.com/samuelvanderwaal/metaplex_decoder_py). Check [releases](https://github.com/samuelvanderwaal/metaplex_decoder_py/releases/).

We can create docker container:

```bash
docker build -t fifund_quests_following_twitter:latest .

docker run -d \
    --name fifund_quest_follow_fifund_twitter \
    -e TWITTER_ACCOUNT="FiFund_io" \
    -e QUEST_ID="fifund_twitter" \
    -e CREDITS=3 \
    -e BOT_LOOP_TIME=450 \
    -e PYTHONUNBUFFERED=1 \
    fifund_quests_following_twitter
```
