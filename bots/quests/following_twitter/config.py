"""
    This file contains the configuration of the arguments and database for the bot
"""
import argparse

import pymongo


""" Arguments """
_args = argparse.ArgumentParser()

_args.add_argument(
    "--twitter_account",
    required=True, 
    help="The Twitter account from which check the followers"
)

_args.add_argument(
    "--quest_id",
    required=True, 
    help="The id of the quest that envolves the bot"
)

_args.add_argument(
    "--credits",
    required=True, 
    help="The amount of credits to give to the user"
)

_args.add_argument(
    "--bot_loop_time",
    required=False, 
    default=5,
    help="Collection from which watch sales and verify at Discord"
)

_args.add_argument(
    "--verbose", 
    action="store_true",
    required=False, default=False, 
    help="Show verbose results (default: %(default)s)"
)
args = _args.parse_args()


""" Database """
DB = "Games"
MONGO_USER = "maparla"
MONGO_PASSWORD = "RI9ARUsN27PjSH25"
MONGO_CLUSTER = "cluster0.fbotufa.mongodb.net"

MONGO_CLIENT = pymongo.MongoClient(f"mongodb+srv://{MONGO_USER}:{MONGO_PASSWORD}@{MONGO_CLUSTER}")

MONGO_DB = MONGO_CLIENT[DB]
USERS_COLLECTION = MONGO_DB["users"]

