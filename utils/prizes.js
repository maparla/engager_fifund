import { BiBomb } from "react-icons/bi"
import { RiPriceTag3Line } from "react-icons/ri"

export const STORE_TYPE_DISPLAY = {
    random_raffle: 'Random Raffle',
    unique_number: 'Unique Number',
    limited_edition: 'Limited Edition',
}

export const STORE_TYPE_SUBMIT = {
    random_raffle: 'Buy Ticket',
    unique_number: 'Submit Number',
    limited_edition: 'Buy Item',
}

export const PRIZES_TO_NAMES = {
    // ========= Common prizes
    "empty": "Ask at Discord",
    "fifund_whitelist": "FiFund Whitelist",
    "fifund_og": "FiFund OG",
    "free_nft": "Free NFT",
    "01_sol": "0.1 SOL",
    "05_sol": "0.5 SOL",
    // ========= Custom prizes
    "nft_degod_1855": "NFT DeGod #1855",
    "fifund_whitelist_v0": "FiFund Whitelist",
}

export const PRIZE_TO_IMAGE = {
    // ========= Common prizes
    "empty": "/images/games/prizes/nothing_banner.png",
    "fifund_whitelist": "/images/store/whitelist_rectangle.png",
    "fifund_og": "/images/store/og_rectangle.png",
    "free_nft": "/images/games/prizes/free_nft_banner.png",
    "01_sol": "/images/games/prizes/01_sol_banner.png",
    "05_sol": "/images/games/prizes/05_sol_banner.png",
    // ========= Custom prizes
    "nft_degod_1855": "/images/store/1855-dead.png",
    "fifund_whitelist_v0": "/images/store/whitelist_rectangle.png",
    "fifund_nft_raffle_0": "/images/store/fifund_nft_raffle_0.png",
}

export const GAME_TO_ICON = {
    // ========= Common prizes
    "card_bomb": <BiBomb />,
    // ========= Custom prizes
    "random_raffle": <RiPriceTag3Line />,
    "limited_edition": <RiPriceTag3Line />,
}

export const GAME_TO_IMAGE = {
    "game_card_bomb": "/images/games/card_bomb/banner.png",
}