export const QUESTS = [
    {
        id: "fifund_discord",
        name: 'FiFund Discord',
        description: 'Join FiFund Discord server',
        image: '/images/quests/fifund_discord.png',
        credits: 3,
    },
    {
        id: "fifund_twitter",
        name: 'FiFund Twitter',
        description: 'Follow @FiFund_io',
        image: '/images/quests/fifund_twitter.jpg',
        credits: 3,
    },
    {
        id: "fifund_docs",
        name: 'FiFund Docs',
        description: 'Read FiFund documentation',
        image: '/images/quests/fifund_docs.jpg',
        credits: 3,
    },
    {
        id: "fifund_discord_invites_5",
        name: '5 Invites',
        description: 'Reach 5 Discord Invites',
        image: '/images/quests/5_invites.png',
        credits: 1,
    },
    {
        id: "fifund_discord_invites_15",
        name: '15 Invites',
        description: 'Reach 15 Discord Invites',
        image: '/images/quests/15_invites.png',
        credits: 4,
    },
    {
        id: "fifund_discord_invites_25",
        name: '25 Invites',
        description: 'Reach 25 Discord Invites',
        image: '/images/quests/25_invites.png',
        credits: 8,
    }
]