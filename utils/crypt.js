import { Cipher, createCipheriv, createDecipheriv, Encoding } from "crypto";

const [key, initVector] = JSON.parse(process.env.CRYPT_KEYS).map(
	(key) => Buffer.from(key, "base64")
);

function crypt(
	cipher,
	data,
	inputType,
	outputType
) {
	return Buffer.concat([
		cipher.update(data, inputType),
		cipher.final(),
	]).toString(outputType);
}

export function encrypt(
	data,
	inputType = "utf8",
	outputType = "base64"
) {
	return crypt(
		createCipheriv("aes-256-ctr", key, initVector),
		data,
		inputType,
		outputType
	);
}

export function decrypt(
	data,
	inputType = "base64",
	outputType = "utf8"
) {
	return crypt(
		createDecipheriv("aes-256-ctr", key, initVector),
		data,
		inputType,
		outputType
	);
}
