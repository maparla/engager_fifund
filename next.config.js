/** @type {import('next').NextConfig} */
const nextConfig = {
  reactStrictMode: true,
  swcMinify: true,
  images: {
    domains: [
      'cdn.discordapp.com',
      'res.cloudinary.com',
      'lh3.googleusercontent.com',
    ],
  },
  env: {
    GAME_NUM_CARD_BOMB: 10,
  },
}

module.exports = nextConfig
