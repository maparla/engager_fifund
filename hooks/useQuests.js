import { useState, useCallback } from "react";
import { toast } from "react-toastify";

import { reloadSession } from "../utils/session";


const useQuests = () => {

  const [perform, setPerform] = useState(false);

  /* Remove wallet to userId entrie */
  const performQuest = useCallback(async (quest_id, twitter_linked = false, cheat_code = "") => {
    setPerform(true);

    let response;

    switch (quest_id) {
      case "cheat_code":
        response = await fetch("/api/quests/cheat_code", {
          method: "POST",
          headers: {
            "Content-Type": "application/json",
          },
          body: JSON.stringify({
            cheat_code,
          }),
        });
        break;

      case "fifund_discord":
        response = await fetch("/api/quests/fifund_discord", {
          method: "GET",
          headers: {
            "Content-Type": "application/json",
          },
        });
        break;
      
      case "fifund_discord_invites_5":
        response = await fetch("/api/quests/fifund_discord_invites", {
          method: "POST",
          headers: {
            "Content-Type": "application/json",
          },
          body: JSON.stringify({
            invites_number: 5,
          }),
        });
        break;

      case "fifund_discord_invites_15":
        response = await fetch("/api/quests/fifund_discord_invites", {
          method: "POST",
          headers: {
            "Content-Type": "application/json",
          },
          body: JSON.stringify({
            invites_number: 15,
          }),
        });
        break;

      case "fifund_discord_invites_25":
        response = await fetch("/api/quests/fifund_discord_invites", {
          method: "POST",
          headers: {
            "Content-Type": "application/json",
          },
          body: JSON.stringify({
            invites_number: 25,
          }),
        });
        break;

      case "fifund_twitter":

        if (!twitter_linked) {
          toast.error("You need to link your twitter account first at Account section");
          setPerform(false);
          return;
        }

        toast.info("We are checking our followers. This can take up to 15 minutes.");
        setPerform(false);
        return;


      case "fifund_docs":
        // opens in new tab FiFund documentation page
        response = await fetch("/api/quests/fifund_docs", {
          method: "GET",
          headers: {
            "Content-Type": "application/json",
          },
        });
        window.open("https://docs.fifund.io/", "_blank");
        break;
        
      default:
        toast.error("Quest not found!");
        break;
    }

    const data = await response.json();

    if (!response.ok) {
      toast.error(data.message);
    } else {

      if (response.status !== 200) {
        toast.warning(data.message);
      } else {
        toast.success(data.message);
        reloadSession();
      }

    }

    setPerform(false);

  }, []);

  return {
    perform,
    performQuest,
  };
};

export default useQuests;
