import { useState, useCallback } from "react";
import { toast } from "react-toastify";

import { reloadSession } from "../utils/session";


const useTwitterLogin = () => {

  const [loading, setLoading] = useState(false);

  const registerIntention = useCallback(async () => {
    setLoading(true);

    let response = await fetch("/api/socials/twitter/intention", {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
      }
    });
    
    const data = await response.json();

    if (!response.ok) {
      toast.error(data.message);
    } else {


      if (response.status !== 200) {
        toast.error(data.message);
      } else {
        
        let oauth_token = data.oauth_token;
        // Redirect to twitter login
        window.location.href = `https://api.twitter.com/oauth/authenticate?oauth_token=${oauth_token}`;
        
      }

    }
    
    setLoading(false);

  }, []);

  const unlinkAction = useCallback(async () => {
    setLoading(true);

    let response = await fetch("/api/socials/twitter/unlink", {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
      }
    });
    
    const data = await response.json();

    if (!response.ok) {
      toast.error(data.message);
    } else {


      if (response.status !== 200) {
        toast.error(data.message);
      } else {
        toast.success(data.message);
        reloadSession();
      }

    }
    
    setLoading(false);

  }, []);

  return {
    loading,
    unlinkAction,
    registerIntention,
  };
};

export default useTwitterLogin;
