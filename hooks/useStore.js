import { useState, useCallback } from "react";
import { toast } from "react-toastify";

import { reloadSession } from "../utils/session";


const useStore = () => {

  const [perform, setPerform] = useState(false);
  const [loading, setLoading] = useState(false);
  const [store, setStore] = useState([]);

  const performStore = useCallback(async (item_type, item_id) => {
    setPerform(true);

    let response;

    switch (item_type) {
      case "random_raffle":
        response = await fetch("/api/store/random_raffle", {
          method: "POST",
          headers: {
            "Content-Type": "application/json",
          },
          body: JSON.stringify({
            item_id
          }),
        });
        break;
      case "unique_number":
        response = await fetch("/api/store/unique_number", {
          method: "POST",
          headers: {
            "Content-Type": "application/json",
          },
          body: JSON.stringify({
            item_id
          }),
        });
        break;

      case "limited_edition":
          response = await fetch("/api/store/limited_edition", {
            method: "POST",
            headers: {
              "Content-Type": "application/json",
            },
            body: JSON.stringify({
              item_id
            }),
          });
          break;

      default:
        setPerform(false);
        toast.error("Store item not found!");
        return;
    }


    if (!response.ok) {
      toast.error(response.message);
    } else {

      const data = await response.json();

      if (response.status !== 200) {
        toast.warning(data.message);
      } else {
        toast.success(data.message);
        reloadSession();
      }

    }

    setPerform(false);

  }, []);


  const loadStore = useCallback(async () => {
    setLoading(true);

    let response = await fetch("/api/store/load_store", {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
      }
    });

    if (!response.ok) {
      toast.error(response.message);
    } else {

      const data = await response.json();

      if (response.status !== 200) {
        toast.warning(data.message);
      } else {
        setStore(data.store);
      }

    }

    setLoading(false);

  }, []);

  return {
    loading,
    perform,
    store,
    loadStore,
    performStore,
  };
};

export default useStore;
