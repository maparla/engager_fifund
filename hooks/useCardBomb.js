import { useState, useCallback } from "react";
import { toast } from "react-toastify";

import { reloadSession } from "../utils/session";


const useCardBomb = () => {

  const [loading, setLoading] = useState(false);

  const revealCard = useCallback(async (card_indx) => {
    setLoading(true);

    const revealPromise = async () => {

      let response = await fetch("/api/games/play/card_bomb", {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify({
          card_indx
        }),
      });

      const data = await response.json();

      if (!response.ok) {
        toast.error(data.message);
      } else {
        if (response.status === 202) {
          toast.error(data.message);
        } else if (response.status === 201) {
          toast.success(data.message);
        } else {
          toast.info(data.message);
        }
        reloadSession();

      }

    }

    toast.promise(revealPromise, {
      pending: "Revealing card..."
    });

    setLoading(false);

  }, []);


  const resetCardGame = useCallback(async () => {
    setLoading(true);

    let response = await fetch("/api/games/reset/card_bomb", {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
      },
    });

    const data = await response.json();

    if (!response.ok) {
      toast.error(data.message);
    } else {

      if (response.status === 200) {
        toast.info(data.message);
      } else {
        toast.error(data.message);
      }

      reloadSession();

    }

    setLoading(false);

  }, []);



  return {
    loading,
    revealCard,
    resetCardGame
  };
};

export default useCardBomb;
