import styles from "./Loader.module.css";

export const Loader = ({
  scale,
  transition_time,
}) => {
  return (
    <div
      style={{
        transform: `scale(${scale})`,
        transition: `transform ${transition_time}s ease-in-out`,
      }}
    >
      <div className={styles.loader}>
        <div className={styles.one}></div>
        <div className={styles.two}></div>
        <div className={styles.three}></div>
        <div className={styles.four}></div>
      </div>
    </div>
  );
};
