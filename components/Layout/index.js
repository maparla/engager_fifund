
import Image from 'next/image';
import Link from 'next/link';

import { signOut, useSession } from "next-auth/react"

import { RiAncientPavilionLine, RiUserLine, RiLogoutCircleLine, RiGamepadLine } from "react-icons/ri";

import { TbBuildingStore, TbMenu2, TbX } from "react-icons/tb";


import styles from './Layout.module.css'
import { useRouter } from 'next/router';
import { useState } from 'react';

export const Layout = ({ children }) => {

    const router = useRouter();
    const [openMenu, setOpenMenu] = useState(false);

    const { data: session, status } = useSession()
    const loading = status === "loading"

    if (loading) {
        return <div>Loading...</div>
    }

    if (status === "unauthenticated") {
        router.push('/');
        return (<></>);
    }

    return (
        <>
        <div 
            className={styles.blackout}
            onClick={() => setOpenMenu(false)}
            style={{ 
                width: openMenu ? "100%" : "0",
                backgroundColor: openMenu ? 'rgba(0,0,0,0.5)' : 'rgba(0,0,0,0)',
                
            }}
        >
        </div>
        <div className={styles.layout}>

            <div className={styles.sidenav}>
                <div className={styles.sidenav_header}>
                    <div className={styles.sidenav_header_logo}>
                        <Image
                            src="/images/nav_logo.jpg"
                            alt="logo"
                            width={75}
                            height={75}
                            className={styles.sidenav_header_logo_image}
                        />
                    </div>
                    <div className={styles.sidenav_header_title}>
                        <h2>Booster</h2>
                        <p>Powered by FiFund</p>
                    </div>
                </div>
                <div className={styles.sidenav_body}>
                    <div className={styles.common_actions}>
                        <Link href="/quests">
                            <div className={`
                        ${styles.sidenav_body_item} 
                        ${router.pathname === '/quests' ? styles.sidenav_body_item_active : ''}
                    `}>
                                <RiAncientPavilionLine className={styles.sidenav_body_item_icon} />
                                Quests
                            </div>
                        </Link>

                        <Link href="/store">
                            <div className={`
                        ${styles.sidenav_body_item} 
                        ${router.pathname === '/store' ? styles.sidenav_body_item_active : ''}
                    `}>
                                <TbBuildingStore className={styles.sidenav_body_item_icon} />
                                Store
                            </div>
                        </Link>

                        <Link href="/play">
                            <div className={`
                        ${styles.sidenav_body_item} 
                        ${router.pathname === '/play' ? styles.sidenav_body_item_active : ''}
                    `}>
                                <RiGamepadLine className={styles.sidenav_body_item_icon} />
                                Play
                            </div>
                        </Link>

                        <Link href="/account">
                            <div className={`
                        ${styles.sidenav_body_item} 
                        ${router.pathname === '/account' ? styles.sidenav_body_item_active : ''}
                    `}>
                                <RiUserLine className={styles.sidenav_body_item_icon} />
                                Account
                            </div>
                        </Link>
                    </div>
                    <div className={styles.sidenav_footer}>
                        <div className={`${styles.credits_info} ${session.credits === 0 ? styles.credits_info_empty : ''}`} onClick={signOut}>
                            Credits: {session.credits}
                        </div>
                        <div
                            className={styles.sidenav_body_item}
                            onClick={() => signOut({ callbackUrl: '/' })}
                        >
                            <RiLogoutCircleLine className={styles.sidenav_body_item_icon} />
                            Logout
                        </div>
                    </div>
                </div>
            </div>

            <div
                className={styles.sidenav_mobile_header}
                onClick={() => setOpenMenu(!openMenu)}
            >
                <TbMenu2 className={styles.menu_icon} />
                <div>
                    <span className={styles.menu_title}>Booster</span><span className={styles.menu_coletilla}>by FiFund</span>
                </div>
            </div>
            <div
                className={styles.sidenav_mobile}
                style={{
                    width: openMenu ? '250px' : '0',
                }}
            >
                <div className={styles.sidenav_mobile_close}>
                    <TbX
                        className={styles.menu_close_icon}
                        onClick={() => setOpenMenu(false)}
                    />
                </div>
                
                <div className={`${styles.sidenav_body} ${styles.sidenav_mobile_body}`}>
                    
                    <div className={styles.common_actions}>
                        <Link href="/quests">
                            <div className={`
                        ${styles.sidenav_body_item} 
                        ${router.pathname === '/quests' ? styles.sidenav_body_item_active : ''}
                    `}>
                                <RiAncientPavilionLine className={styles.sidenav_body_item_icon} />
                                Quests
                            </div>
                        </Link>

                        <Link href="/store">
                            <div className={`
                        ${styles.sidenav_body_item} 
                        ${router.pathname === '/store' ? styles.sidenav_body_item_active : ''}
                    `}>
                                <TbBuildingStore className={styles.sidenav_body_item_icon} />
                                Store
                            </div>
                        </Link>

                        <Link href="/play">
                            <div className={`
                        ${styles.sidenav_body_item} 
                        ${router.pathname === '/play' ? styles.sidenav_body_item_active : ''}
                    `}>
                                <RiGamepadLine className={styles.sidenav_body_item_icon} />
                                Play
                            </div>
                        </Link>

                        <Link href="/account">
                            <div className={`
                        ${styles.sidenav_body_item} 
                        ${router.pathname === '/account' ? styles.sidenav_body_item_active : ''}
                    `}>
                                <RiUserLine className={styles.sidenav_body_item_icon} />
                                Account
                            </div>
                        </Link>
                    </div>
                    <div className={styles.sidenav_footer}>
                        <div className={`${styles.credits_info} ${session.credits === 0 ? styles.credits_info_empty : ''}`} onClick={signOut}>
                            Credits: {session.credits}
                        </div>
                        <div
                            className={styles.sidenav_body_item}
                            onClick={() => signOut({ callbackUrl: '/' })}
                        >
                            <RiLogoutCircleLine className={styles.sidenav_body_item_icon} />
                            Logout
                        </div>
                    </div>
                </div>
            </div>

            <div className={styles.main_content}>{children}</div>
        </div>
        </>
    );
};
