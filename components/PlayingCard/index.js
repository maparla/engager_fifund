import { useSession } from "next-auth/react";
import Image from "next/image"
import { useState } from "react"
import useCardBomb from "../../hooks/useCardBomb";

import styles from './PlayingCard.module.css'

const time_display_image = {
    "0": '/images/games/card_bomb/0.svg',
    "1": '/images/games/card_bomb/0.svg',
    "2": '/images/games/card_bomb/0.svg',
    "3": '/images/games/card_bomb/0.svg',
    "4": '/images/games/card_bomb/0.svg',
    "5": '/images/games/card_bomb/0.svg',
    "6": '/images/games/card_bomb/0.svg',
    "7": '/images/games/card_bomb/0.svg',
    "8": '/images/games/card_bomb/0.svg',
    "bomb": '/images/games/card_bomb/bomb.svg',
}

export const PlayingCard = ({ card }) => {

    const { data: session } = useSession();
    const [revealed, setRevealed] = useState(false)

    const {
        loading: revealingCard,
        revealCard,
    } = useCardBomb();

    /*
    useState(() => {
        if (session && session.game_card_bomb_movements) {
            setRevealed(session.game_card_bomb_movements.includes(card.index))
        }
    }, [session, session.game_card_bomb_movements]);
    */

    if (!revealed && session && session.game_card_bomb_movements && session.game_card_bomb_movements.includes(card.index)) {
        setRevealed(true)
    } else if (revealed && session && session.game_card_bomb_movements && !session.game_card_bomb_movements.includes(card.index)) {
        setRevealed(false)
    }

    let isGameLive = session && session.game_card_bomb_result !== 'win' && session.game_card_bomb_result !== 'lose' && (session.game_card_bomb_result === 'playing' || session.credits > 0);

    let card_time = (
        session &&
        session.game_card_bomb_movements
    ) ? session.game_card_bomb_movements.indexOf(card.index) : -1;

    // Ceck if game is lose and card is bomb (card index at last position)
    let loserIndex = (
        session &&
        session.game_card_bomb_result === 'lose' &&
        session.game_card_bomb_movements.length > 0
    ) ? session.game_card_bomb_movements[session.game_card_bomb_movements.length -1 ] : -99;

    let display_time = loserIndex === card.index ? 'bomb' : card_time;

    return (
        <div
            className={styles.flip_card}
            onClick={async () => {
                if (isGameLive && !revealed) {
                    await revealCard(card.index);
                    setRevealed(true);
                }
            }}
        >
            <div
                className={`${styles.flip_card_inner} ${revealed ? styles.revealed : ''} ${!isGameLive ? styles.finished : ''}`}
                style={{
                    transform: revealed ? 'rotateY(180deg)' : 'rotateY(0deg)',
                    transition: revealed ? 'transform 0.65s' : 'transform 1s',
                    cursor: isGameLive && !revealed ? 'pointer' : 'default',
                }}
            >
                <div className={styles.flip_card_front}>
                    <Image
                        src="/images/playing-card-back.png"
                        alt="Card Back"
                        layout="fill"
                        objectFit="cover"
                    />
                </div>
                <div className={`${styles.flip_card_back} ${display_time === 'bomb' ? styles.bomb : ''}`}>
                    <div className={styles.card_time_image_container}>
                        {card_time >= 0 &&
                            <Image
                                src={time_display_image[display_time]}
                                alt="Card Result"
                                layout="fill"
                                objectFit="contain"
                                className={styles.card_time_image}
                            />
                        }
                    </div>
                </div>
            </div>
        </div>
    )
}