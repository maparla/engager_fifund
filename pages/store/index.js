import { useSession } from "next-auth/react"

import styles from './Store.module.css'
import { Layout } from '../../components/Layout'
import Image from "next/image"

import { RiTrophyLine, RiCalendarTodoFill, RiBarChart2Line, RiPriceTag3Line } from "react-icons/ri";
import { TbTicket } from "react-icons/tb";
import useStore from "../../hooks/useStore";
import { Loader } from "../../components/Loader/Loader";
import Link from "next/link";
import moment from "moment";
import { useEffect, useState } from "react";
import { STORE_TYPE_DISPLAY, STORE_TYPE_SUBMIT } from "../../utils/prizes";


export default function Play() {

    const [displayItemId, setDisplayItemId] = useState("");

    const { data: session } = useSession();

    const {
        loading: loadingStore,
        perform: performingStores,
        store,
        loadStore,
        performStore,
    } = useStore();

    // Load the store once the user is logged in
    useEffect(() => {
        if (session) {
            loadStore();
        }
    }, [session, loadStore]);

    // Order strore by status and date
    const orderedStore = store.sort((a, b) => {
        if (a.status === b.status) {
            return new Date(a.end_date) > new Date(b.end_date) ? 1 : -1;
        } else {
            return a.status ? -1 : 1;
        }
    });

    // check if displayItemId is at orderdStore and if it is set the displayItemId to the id
    let item_info;
    if (orderedStore.find(item => item._id === displayItemId)) {
        item_info = orderedStore.find(item => item._id === displayItemId);
    }

    let can_purchase_item = false;
    let date_passed = false;
    if (session && item_info) {
        date_passed = moment(moment(new Date(item_info.end_date))).isBefore(moment());
        can_purchase_item = (session.credits >= item_info.price) && !date_passed && item_info.status;
    }

    return (

        <>

            {item_info !== undefined && (

                <div
                    className={styles.modal_item_backdrop}
                    onClick={() => setDisplayItemId("")}
                >
                    <div
                        className={styles.modal_item}
                        onClick={e => e.stopPropagation()}
                    >
                        <div className={styles.store_image_container}>
                            <Image
                                src={`/images/store/${item_info.image}`}
                                alt={item_info.name}
                                layout="fill"
                                objectFit="cover"
                                className={styles.store_image}
                            />
                        </div>
                        <div className={styles.item_details}>
                            <div className={styles.item_header}>
                                <h1>{item_info.name}</h1>
                                <p>{item_info.description}</p>
                            </div>
                            <div className={styles.item_meta}>
                                {
                                    item_info.type === "random_raffle" && (
                                        <>
                                            <p><TbTicket className={styles.item_meta_icon} /> Your tickets: {item_info.user_entries}</p>
                                            <p><RiBarChart2Line className={styles.item_meta_icon} />Tickets sold: {item_info.total_entries}</p>
                                            <p><RiTrophyLine className={styles.item_meta_icon} /> Total prizes: {item_info.prizes}</p>
                                            <p><RiPriceTag3Line className={styles.item_meta_icon} /> Cost: {item_info.price} credits</p>
                                        </>
                                    )
                                }
                                {
                                    item_info.type === "limited_edition" && (
                                        <>
                                            <p><RiTrophyLine className={styles.item_meta_icon} /> Items left: {item_info.prizes}</p>
                                            <p><RiPriceTag3Line className={styles.item_meta_icon} /> Cost: {item_info.price} credits</p>
                                        </>
                                    )
                                }
                            </div>

                            {item_info.winner ? (
                                <div className={styles.won_btn}>
                                    {
                                        item_info.type === "random_raffle" && (
                                            <>
                                                You won
                                            </>
                                        )
                                    }
                                    {
                                        item_info.type === "limited_edition" && (
                                            <>
                                                Purchased
                                            </>
                                        )
                                    }

                                </div>
                            ) : (
                                <div
                                    className={styles.item_purchase_btn}
                                    style={{
                                        cursor: can_purchase_item ? 'pointer' : 'not-allowed',
                                        filter: can_purchase_item ? 'none' : 'grayscale(100%)',
                                    }}
                                    onClick={() => {
                                        if (can_purchase_item) {
                                            setDisplayItemId("");
                                            performStore(item_info.type, item_info._id);
                                        }
                                    }}
                                >
                                    {item_info.status && !date_passed && STORE_TYPE_SUBMIT[item_info.type]}
                                    {item_info.status && date_passed && "To be raffled"}
                                    {!item_info.status && date_passed && "No winnings"}
                                    {!item_info.status && !date_passed && "Not available"}
                                </div>
                            )}


                        </div>
                    </div>
                </div>
            )}

            <Layout>
                <div className={styles.store_container}>

                    <h1>Store</h1>

                    <div className={styles.description}>
                        <p>Checkout our store for various items and services.</p>
                        <p>Remember that you can review your purchase history, and prizes, at your <Link href='/account'><b style={{ cursor: 'pointer' }} >Account section</b></Link>.</p>
                    </div>

                    {loadingStore && store.length === 0 && <div>Loading Store...</div>}
                    {loadingStore && store.length > 0 && <div>Updating Store...</div>}

                    <div className={styles.stores}>
                        {orderedStore.map((item, index) => (
                            <div
                                className={`${styles.store} ${item.status ? styles.store_is_completed : ""} ${item.winner ? styles.winner_glow : ""}`}
                                key={index}

                                onClick={() => {
                                    setDisplayItemId(item._id);
                                }}
                            >
                                <div className={styles.store_image_container}>
                                    <Image
                                        src={`/images/store/${item.image}`}
                                        alt={item.name}
                                        layout="fill"
                                        objectFit="cover"
                                        className={styles.store_image}
                                        style={{
                                            filter: item.status && moment(moment()).isBefore(moment(new Date(item.end_date))) ? 'none' : 'grayscale(100%)',
                                        }}
                                    />
                                </div>
                                <div className={styles.store_meta}>
                                    <div className={styles.store_info}>
                                        <div className={styles.store_name}>
                                            <h1>{item.name}</h1>
                                        </div>
                                        <div className={styles.store_description}>
                                            <p>{item.description}</p>
                                            <p>{STORE_TYPE_DISPLAY[item.type]}</p>

                                            <div className={styles.date_info}>
                                                <RiCalendarTodoFill className={styles.calendar_icon} />
                                                {moment(moment(new Date(item.end_date))).isBefore(moment()) ? '[Closed]' : '[Live]'} {moment(new Date(item.end_date)).fromNow()}
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        ))}
                    </div>

                    {performingStores && (
                        <div className={styles.loader_container}>
                            <Loader scale={0.25} transition_time={0.5} />
                        </div>
                    )}

                </div>

            </Layout>

        </>
    )
}
