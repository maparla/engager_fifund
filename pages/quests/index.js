import { useSession } from "next-auth/react"

import styles from './Quests.module.css'
import { Layout } from '../../components/Layout'
import Image from "next/image"

import { RiTrophyLine, RiPlayLine } from "react-icons/ri";
import { TbHourglassLow, TbTicket } from "react-icons/tb";
import useQuests from "../../hooks/useQuests";
import { Loader } from "../../components/Loader/Loader";
import { QUESTS } from "../../utils/quests";


export default function Play() {

    const { data: session } = useSession()

    const {
        perform: performingQuests,
        performQuest,
    } = useQuests();

    // Set the quest status once user session is loaded
    if (session) {
        QUESTS.forEach(quest => {
            if (session.pending_quests && session.pending_quests.includes(quest.id)) {
                quest.status = "pending";
            }
            else if (session.completed_quests && session.completed_quests.includes(quest.id)) {
                quest.status = "completed";
            } else {
                quest.status = "none";
            }
        });
    }

    // Order quests by status and credits
    QUESTS.sort((a, b) => {
        if (a.status === b.status) {
            return b.credits - a.credits;
        } else {
            return a.status === "none" ? -1 : 1;
        }
    });
    

    return (
        <Layout>
            <div className={styles.quest_container}>

                <h1>Quests</h1>

                <div className={styles.description}>
                    <p>Theses are the quests you can play.</p>
                    <p>Each quest will give you credits to spend on other playing games that can give you whitelist access, NFTs or even SOL.</p>
                </div>

                <div className={styles.quests}>
                    {QUESTS.map(quest => (
                        <div
                            className={`${styles.quest} ${quest.status === "completed" ? styles.quest_is_completed : ""}`}
                            key={quest.id}
                            style={{
                                cursor: quest.status == "none" ? 'pointer' : 'default',
                                filter: quest.status == "completed" ? 'grayscale(100%)' : 'none',
                            }}
                            onClick={() => {
                                if (quest.status == "none") {
                                    performQuest(
                                        quest.id,
                                        session?.twitter_id ? true : false
                                    );
                                }
                            }}
                        >
                            <div className={styles.quest_image_container}>
                                <Image
                                    src={quest.image}
                                    alt={quest.name}
                                    layout="fill"
                                    objectFit="cover"
                                    className={styles.quest_image}
                                />
                            </div>
                            <div className={styles.quest_meta}>
                                <div className={styles.quest_info}>
                                    <div className={styles.quest_name}>
                                        <h1>{quest.name}</h1>
                                    </div>
                                    <div className={styles.quest_description}>
                                        <p>{quest.description}</p>
                                    </div>
                                </div>
                                <div className={styles.quest_completed}>
                                    {quest.status == "completed" ? (
                                        <div className={styles.quest_completed_icon}>
                                            <RiTrophyLine className={styles.quest_status_icon} />
                                        </div>
                                    ) : quest.status == "pending" ?
                                        <div className={styles.quest_completed_icon}>
                                            <TbHourglassLow className={styles.quest_status_icon} />
                                        </div> : (
                                            <div className={styles.quest_completed_icon}>
                                                <RiPlayLine className={styles.quest_status_icon} />
                                            </div>
                                        )}
                                    <div className={styles.quest_credits}>
                                        <TbTicket className={styles.credits_icon} /> {quest.credits}
                                    </div>
                                </div>
                            </div>
                        </div>
                    ))}
                </div>

                {performingQuests && (
                    <div className={styles.loader_container}>
                        <Loader scale={0.25} transition_time={0.5} />
                    </div>
                )}

            </div>
        </Layout>
    )
}
