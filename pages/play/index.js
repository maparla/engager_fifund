import styles from './Play.module.css'
import { PlayingCard } from '../../components/PlayingCard'
import { Layout } from '../../components/Layout'
import { useSession } from 'next-auth/react'

import { RiEmotionUnhappyLine, RiEmotionHappyLine } from "react-icons/ri";
import { TbHourglassLow, TbArrowBackUp } from "react-icons/tb";
import useCardBomb from '../../hooks/useCardBomb';
import Link from 'next/link';
import { reloadSession } from '../../utils/session';
import { PRIZES_TO_NAMES } from '../../utils/prizes';
import { useState } from 'react';
import Image from 'next/image';

export default function Play() {

    const { data: session } = useSession();

    const [displayReward, setDisplayReward] = useState(true);

    const {
        loading: revealingCard,
        revealCard,
        resetCardGame
    } = useCardBomb();

    let card_bomb_prizes = session?.prizes?.filter(prize => prize.game === "card_bomb") || []
    // Get the last reward for prizes with id card_bomb
    let last_reward = session && session.game_card_bomb_result === 'win' && card_bomb_prizes.length > 0 ? card_bomb_prizes[card_bomb_prizes.length - 1] : null;

    return (
        <>

            {displayReward && last_reward && last_reward.type !== "nothing" && (
                <div
                    className={styles.modal_reward_backdrop}
                    onClick={() => setDisplayReward(false)}
                >
                    <div
                        className={styles.modal_reward}
                        onClick={e => e.stopPropagation()}
                    >
                        <h1>Congrats, you won!</h1>
                        <div className={styles.reward_info_image}>
                            <Image
                                src='/images/games/prizes/trophy.svg'
                                alt="Card Result"
                                layout="fill"
                                objectFit="contain"
                            />
                        </div>
                        <p className={styles.reward_info}>You have won <b>{PRIZES_TO_NAMES[last_reward?.type]}</b></p>
                        <p><i>To claim send a screenshot to the quests-proofs channel in our discord</i></p>
                    </div>
                </div>
            )}

            {displayReward && last_reward && last_reward.type === "nothing" && (
                <div
                    className={styles.modal_reward_backdrop}
                    onClick={() => setDisplayReward(false)}
                >
                    <div
                        className={styles.modal_reward}
                        onClick={e => e.stopPropagation()}
                    >
                        <h1>You won but...</h1>
                        <div className={styles.reward_info_image}>
                            <Image
                                src='/images/games/prizes/nothing.svg'
                                alt="Card Result"
                                layout="fill"
                                objectFit="contain"
                            />
                        </div>
                        <p className={styles.reward_info}>You had no luck this time. Keep trying!</p>
                    </div>
                </div>
            )}

            <Layout>
                <div className={styles.container}>

                    <div className={styles.header}>
                        <h1>Kaboom!</h1>
                        <p>Don&apos;t turn <b>the bomb</b>. <i>Easy</i>. Each game costs 1 credit.</p>
                        <p>Awards can range from whitelist, free NFT, a variable amount of SOL...</p>
                        <p>To start playing, <b>click on a card to flip it over.</b></p>
                    </div>

                    <div className={styles.card_game}>

                        <div className={styles.deck}>

                            {
                                /* Crate as much as PlayingCard as process.env.GAME_NUM_CARD_BOMB*/
                                Array(parseInt(process.env.GAME_NUM_CARD_BOMB))
                                    .fill(0)
                                    .map((_, index) => (
                                        <PlayingCard
                                            key={index}
                                            card={{ index: index }}
                                        />
                                    ))
                            }

                        </div>

                    </div>

                    <div className={styles.footer}>
                        {session && ((session.game_card_bomb_result === 'win' || session.game_card_bomb_result === 'lose')) && session.credits > 0 && (
                            <div
                                className={styles.retry}
                                onClick={async () => {
                                    await resetCardGame();
                                    reloadSession();
                                }}
                            >
                                {session.game_card_bomb_result === 'win' && "Play Again"}
                                {session.game_card_bomb_result === 'lose' && "Try Again"}
                                <TbArrowBackUp className={styles.retry_icon} />
                            </div>
                        )}

                        {session && session.credits === 0 && session.game_card_bomb_result !== 'playing' && (
                            <div className={styles.no_credits}>
                                <p>Oh no, you don&apos;t have any credits <RiEmotionUnhappyLine className={styles.no_credits_icon} /></p>
                                <p>You can earn more credits at <Link href='/quests'><b style={{ cursor: 'pointer' }} >Quests section</b></Link>.</p>
                            </div>
                        )}

                    </div>

                    <div className={styles.game_status}>

                        {session && session.game_card_bomb_result === 'win' && (
                            <div className={styles.game_status_won}>
                                <RiEmotionHappyLine className={styles.status_icon} />
                                <span>You won!</span>
                            </div>
                        )}

                        {session && session.game_card_bomb_result === 'lose' && (
                            <div className={styles.game_status_lost}>
                                <RiEmotionUnhappyLine className={styles.status_icon} />
                                <span>You lost!</span>
                            </div>
                        )}
                        {session && session.game_card_bomb_result === 'playing' && (
                            <div className={styles.game_status_playing}>
                                <TbHourglassLow className={styles.status_icon} />
                                <span>Playing!</span>
                            </div>
                        )}

                    </div>

                </div>

            </Layout>
        </>
    )
}
