import { SessionProvider } from "next-auth/react"
import Head from "next/head"
import '../styles/globals.css'

import { ToastContainer } from "react-toastify";

import "react-toastify/dist/ReactToastify.css";

// Use of the <SessionProvider> is mandatory to allow components that call
// `useSession()` anywhere in your application to access the `session` object.
export default function MyApp({ Component, pageProps: { session, ...pageProps }, router }) {
  return (
    <SessionProvider session={session} refetchInterval={0}>
        {/*
        <Head>
          <meta name="description" content="MukyLabs. The tools every community needs. Holders verification, rarity bot and sales bots for your Solana project." />
          <meta property="og:type" content="website" />
          <meta property="og:url" content="https://mukylabs.com/" />
          <meta property="og:title" content="MukyLabs: Solana Swiss Services" />
          <meta property="og:description" content="MukyLabs. The tools every community needs. Holders verification, rarity bot and sales bots for your Solana project." />
          <meta property="og:image" content="https://i.imgur.com/WyNJxgI.jpg" />

          <link rel="icon" href="/images/logo/favicon_64.png" />
        </Head>
        */}
        <Component {...pageProps} />
        <ToastContainer
          position="bottom-right"
          autoClose={2500}
          hideProgressBar={false}
          newestOnTop={false}
          closeOnClick
          rtl={false}
          pauseOnFocusLoss
          draggable
          pauseOnHover
        />
    </SessionProvider>
  )
}