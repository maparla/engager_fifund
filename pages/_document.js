// pages/_document.js

import Document, { Html, Head, Main, NextScript } from 'next/document'

class MyDocument extends Document {
    render() {
        return (
            <Html>
                <Head>
                <title>Booster by FiFund</title>
                <meta name="description" content="Booster app by FiFund" />

                    <link rel="preconnect" href="https://fonts.googleapis.com" />
                    <link rel="preconnect" href="https://fonts.gstatic.com" />
                    <link href="https://fonts.googleapis.com/css2?family=Mulish:wght@900&family=Work+Sans:wght@400;600&display=swap" rel="stylesheet" />
                    <link rel="icon" type="image/x-icon" href="/images/FF-Favicon-32x32.png" />
                </Head>
                <body>
                    <Main />
                    <NextScript />
                </body>
            </Html>
        )
    }
}

export default MyDocument