// https://github.com/UnusualAbsurd/discord-oauth/blob/main/src/pages/api/auth/callback.ts
// Next.js API route support: https://nextjs.org/docs/api-routes/introduction
import { MongoClient, ObjectId } from 'mongodb';
import { getSession } from "next-auth/react"

import { decrypt } from "../../../../utils/crypt";

const DB_NAME = 'Games';
const USERS_COLLETION_NAME = "users";

export default async function handler(req, res) {

    const session = await getSession({ req })
    if (!session) {
        res.status(401).json({ message: "User not signed in" });
        return
    }

    let userId = session.id;

    if (userId === undefined) {
        res.status(400).json("Malformed data");
        return;
    }

    userId = ObjectId(decrypt(userId));

    const client = await MongoClient.connect(
        `mongodb+srv://${process.env.MONGO_USERNAME}:${process.env.MONGO_PASSWORD}@${process.env.MONGO_CLUSTER}/${DB_NAME}?retryWrites=true&w=majority`
    );

    // Recordad que deberiamos comprobar si hay errores en alguno de los puntos
    const db = client.db();

    const usersCollection = db.collection(USERS_COLLETION_NAME);
    const user = await usersCollection.findOne({
        _id: userId
    });

    if (user === null) {
        client.close();
        res.status(404).json({ message: "User not found" });
        return;
    }

    // ================================================================
    // ================================================================

    try {
        const { oauth_token: req_oauth_token, oauth_verifier } = req.query;

        let access = await fetch(`https://api.twitter.com/oauth/access_token?oauth_verifier=${oauth_verifier}&oauth_token=${req_oauth_token}`, {
            method: "POST",
        });

        let access_data = await access.text();
        access_data = new URLSearchParams(access_data);

        let user_id = access_data.get('user_id');
        let screen_name = access_data.get('screen_name');

        // Insert the information at user profile
        await usersCollection.updateOne({
            _id: userId
        }, {
            $set: {
                "socials.twitter.oauth_verifier": oauth_verifier,
                "socials.twitter.user_id": user_id,
                "socials.twitter.screen_name": screen_name
            }
        });

    } catch (error) {
        res.status(403).json({ message: "Missing access token" });
    }

    client.close();
    res.redirect("/account");

}
