// Utilizando Atlas de https://cloud.mongodb.com/
import { MongoClient, ObjectId } from 'mongodb';
import { decrypt } from '../../../../utils/crypt';

const DB_NAME = 'Games';
const USERS_COLLETION_NAME = "users";

import { getSession } from "next-auth/react";
import { OAuth } from 'oauth';

const CONSUMER_KEY = "Jz7lkOhgrbs6Mhre8BX008ula"
const CONSUMER_SECRET = "mHusXri4GtcSKJs0iCzSh3qfFOdl7Y81zq3DI9MMHtBHBLCcSE"


// const oauthCallback = "http://localhost:8129/api/socials/twitter/register"
const oauthCallback = "https://booster.fifund.io/api/socials/twitter/register"



const _oauth = new OAuth(
    'https://api.twitter.com/oauth/request_token',
    'https://api.twitter.com/oauth/access_token',
    CONSUMER_KEY, // consumer key
    CONSUMER_SECRET, // consumer secret
    '1.0',
    oauthCallback,
    'HMAC-SHA1'
);

const getOAuthRequestToken = () => {
    return new Promise((resolve, reject) => {
        _oauth.getOAuthRequestToken((error, oauth_token, oauth_token_secret, results) => {
            if (error) {
                reject(error);
            } else {
                resolve({ oauth_token, oauth_token_secret, results });
            }
        });
    });
}

async function twitterLoginIntention(req, res) {

    if (req.method !== "GET") {
        res.status(405).json("Method not allowed");
        return;
    }

    const session = await getSession({ req })
    if (!session) {
        res.status(401).json({ message: "User not signed in" });
        return
    }

    let userId = session.id;

    if (userId === undefined) {
        res.status(400).json("Malformed data");
        return;
    }

    userId = ObjectId(decrypt(userId));

    const client = await MongoClient.connect(
        `mongodb+srv://${process.env.MONGO_USERNAME}:${process.env.MONGO_PASSWORD}@${process.env.MONGO_CLUSTER}/${DB_NAME}?retryWrites=true&w=majority`
    );

    // Recordad que deberiamos comprobar si hay errores en alguno de los puntos
    const db = client.db();

    const usersCollection = db.collection(USERS_COLLETION_NAME);
    const user = await usersCollection.findOne({
        _id: userId
    });

    if (user === null) {
        client.close();
        res.status(404).json({ message: "User not found" });
        return;
    }

    // ================================================================
    // ================================================================


    try {
        //const { oauth_token, oauth_token_secret } = await getOAuthRequestToken();
        const { oauth_token, oauth_token_secret, results } = await getOAuthRequestToken();

        if (!results.oauth_callback_confirmed) {
            client.close();
            res.status(400).json({ message: "OAuth callback not confirmed" });
            return;
        }

        // ================================================================
        // Añadimos al usuario el token y el secreto de OAuth
        await usersCollection.updateOne(
            { _id: userId },
            {
                $set: {
                    "socials.twitter.token": oauth_token,
                    "socials.twitter.tokenSecret": oauth_token_secret
                }
            }
        );

        client.close();

        res.status(200).json({ oauth_token: oauth_token });

    } catch (error) {

        client.close();

        res.status(401).json({ message: "Unable to link. Please try again" });
        return;

    }

}

export default twitterLoginIntention;