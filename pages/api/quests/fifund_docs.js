// Utilizando Atlas de https://cloud.mongodb.com/
import { MongoClient, ObjectId } from 'mongodb';
import { decrypt } from '../../../utils/crypt';

const DB_NAME = 'Games';
const USERS_COLLETION_NAME = "users";

import { getSession } from "next-auth/react";

async function fifundDocsQuest(req, res) {

  if (req.method !== "GET") {
    res.status(405).json("Method not allowed");
    return;
  }

  const session = await getSession({ req })
  if (!session) {
    res.status(401).json({ message: "User not signed in" });
    return
  }

  let userId = session.id;

  if (userId === undefined) {
    res.status(400).json("Malformed data");
    return;
  }

  userId = ObjectId(decrypt(userId));

  const client = await MongoClient.connect(
    `mongodb+srv://${process.env.MONGO_USERNAME}:${process.env.MONGO_PASSWORD}@${process.env.MONGO_CLUSTER}/${DB_NAME}?retryWrites=true&w=majority`
  );

  // Recordad que deberiamos comprobar si hay errores en alguno de los puntos
  const db = client.db();

  const usersCollection = db.collection(USERS_COLLETION_NAME);
  const user = await usersCollection.findOne({
    _id: userId
  });

  if (user === null) {
    client.close();
    res.status(404).json({ message: "User not found" });
    return;
  }

  // ================================================================
  // ================================================================

  // Si el usuario ya tiene el quest completado, no hacemos nada
  const user_all_quests = user.pendings_quests || [] + user.completed_quests || [];
  if (user_all_quests.includes("fifund_docs")) {
    client.close();
    res.status(202).json({ message: "Quest already completed" });
    return;
  }

  // Si el usuario no tiene el quest pendiente, lo añadimos
  // y añadimos un credito adicional a lo que ya puede tener
  // udapte user by id
  await usersCollection.updateOne(
    { _id: userId },
    {
      $push: {
        completed_quests: "fifund_docs"
      },
      $inc: {
        credits: 3
      }
    }
  );

  client.close();

  res.status(200).json({ message: "Quest completed!" });
}

export default fifundDocsQuest;