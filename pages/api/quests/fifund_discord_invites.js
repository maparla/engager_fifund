// Utilizando Atlas de https://cloud.mongodb.com/
import { MongoClient, ObjectId } from 'mongodb';
import { decrypt } from '../../../utils/crypt';

const DB_NAME = 'Games';
const USERS_COLLETION_NAME = "users";
const ACCOUNTS_COLLETION_NAME = "accounts";

import { getSession } from "next-auth/react";

import { Client, GatewayIntentBits } from "discord.js";

const INVITES_CREDITS = {
  5: 1,
  15: 4,
  25: 8
}

async function fifundDocsQuest(req, res) {

  if (req.method !== "POST") {
    res.status(405).json("Method not allowed");
    return;
  }

  const session = await getSession({ req })
  if (!session) {
    res.status(401).json({ message: "User not signed in" });
    return
  }

  let userId = session.id;

  if (userId === undefined) {
    res.status(400).json("Malformed data");
    return;
  }

  userId = ObjectId(decrypt(userId));

  const client = await MongoClient.connect(
    `mongodb+srv://${process.env.MONGO_USERNAME}:${process.env.MONGO_PASSWORD}@${process.env.MONGO_CLUSTER}/${DB_NAME}?retryWrites=true&w=majority`
  );

  // Recordad que deberiamos comprobar si hay errores en alguno de los puntos
  const db = client.db();

  const usersCollection = db.collection(USERS_COLLETION_NAME);
  const user = await usersCollection.findOne({
    _id: userId
  });

  if (user === null) {
    client.close();
    res.status(404).json({ message: "User not found" });
    return;
  }

  let { invites_number } = req.body;

  // ================================================================
  // ================================================================

  // Comprobamos que el invites_number es correcto
  if (!INVITES_CREDITS[invites_number]) {
    client.close();
    res.status(400).json("Malformed data");
    return;
  }

  const quest_name = `fifund_discord_invites_${invites_number}`;

  // Si el usuario ya tiene el quest completado, no hacemos nada
  const user_all_quests = user.pendings_quests || [] + user.completed_quests || [];
  if (user_all_quests.includes(quest_name)) {
    client.close();
    res.status(202).json({ message: "Quest already completed" });
    return;
  }


  // ================================================================
  // ================================================================

  // Comprobamos si el usuario esta en discord o no

  const discordClient = new Client({
    intents: [
      GatewayIntentBits.Guilds,
      GatewayIntentBits.GuildMembers,
      GatewayIntentBits.GuildInvites
    ]
  });

  await discordClient.login(process.env.DISCORD_SERVER_BOT_TOKEN);
  const fifundGuild = await discordClient.guilds.fetch(process.env.DISCORD_FIFUND_GUILD);

  if (!fifundGuild) {
    client.close();
    res.status(404).json({ message: "Unable to fetch FiFund Discord Guild" });
    return;
  }

  var guild_members = await fifundGuild.members.fetch();

  if (!guild_members) {
    client.close();
    res.status(404).json({ message: "Unable to fetch FiFund Discord Members" });
    return;
  }
  let guild_members_ids = guild_members.map(member => member.user.id);

  const accountsCollection = db.collection(ACCOUNTS_COLLETION_NAME);
  const account = await accountsCollection.findOne({
    userId: userId
  });

  if (account === null) {
    client.close();
    res.status(400).json({ message: "User not found" });
    return;
  }

  let discordId = account.providerAccountId;

  if (guild_members_ids.indexOf(discordId) === -1) {
    client.close();
    res.status(400).json({ message: "User not in FiFund Discord Guild" });
    return;
  }

  // ================================================================
  // ================================================================

  // Obtenemos el numero de invites del usuario
  const invites = await fifundGuild.invites.fetch({cache: false});
  const user_invites = invites.filter(invite => invite.inviterId === discordId);
  let user_invites_number = 0;
  for (const [invite_code, invite_info] of user_invites) {
    user_invites_number += invite_info.uses;
  }

  if (user_invites_number === undefined || user_invites_number < invites_number) {
    client.close();
    res.status(400).json({ message: "User does not have enough invites" });
    return;
  }

  // ================================================================
  // ================================================================

  // El user ha pasado todos los checks, udapte user by id
  await usersCollection.updateOne(
    { _id: userId },
    {
      $push: {
        completed_quests: quest_name
      },
      $inc: {
        credits: INVITES_CREDITS[invites_number]
      }
    }
  );

  client.close();

  res.status(200).json({ message: "Quest completed!" });
}

export default fifundDocsQuest;