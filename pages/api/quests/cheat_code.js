// Utilizando Atlas de https://cloud.mongodb.com/
import { MongoClient, ObjectId } from 'mongodb';
import { decrypt } from '../../../utils/crypt';

const DB_NAME = 'Games';
const USERS_COLLETION_NAME = "users";
const META_COLLETION_NAME = "meta";

import { getSession } from "next-auth/react";

async function CheatCodes(req, res) {

  if (req.method !== "POST") {
    res.status(405).json("Method not allowed");
    return;
  }

  const session = await getSession({ req })
  if (!session) {
    res.status(401).json({ message: "User not signed in" });
    return
  }

  let userId = session.id;

  if (userId === undefined) {
    res.status(400).json("Malformed data");
    return;
  }

  userId = ObjectId(decrypt(userId));

  const client = await MongoClient.connect(
    `mongodb+srv://${process.env.MONGO_USERNAME}:${process.env.MONGO_PASSWORD}@${process.env.MONGO_CLUSTER}/${DB_NAME}?retryWrites=true&w=majority`
  );

  // Recordad que deberiamos comprobar si hay errores en alguno de los puntos
  const db = client.db();

  const usersCollection = db.collection(USERS_COLLETION_NAME);
  const user = await usersCollection.findOne({
    _id: userId
  });

  if (user === null) {
    client.close();
    res.status(404).json({ message: "User not found" });
    return;
  }

  let { cheat_code } = req.body;

  // remove spaces and make it lowercase
  cheat_code = cheat_code.replace(/\s/g, '').toLowerCase();

  // ================================================================
  // ================================================================

  // Si el usuario ya tiene el cheat_code canjeado
  const user_all_cheat_codes = user.cheat_codes || [];
  if (user_all_cheat_codes.includes(cheat_code)) {
    client.close();
    res.status(400).json({ message: "Cheat code already redeemed" });
    return;
  }


  // Ahora vamos a comprobar si el cheat_code es valido
  const metaCollection = db.collection(META_COLLETION_NAME);
  const cheatMeta = await metaCollection.findOne({
    id: cheat_code
  });

  if (cheatMeta === null) {
    client.close();
    res.status(500).json({ message: "Cheat code not found" });
    return;
  }

  /* cheatMeta has the following structure:
      {
        "_id":{
            "$oid":"63132a7af147f1ad44b41d39"
        },
        "id":"first10",
        "type":"cheat_code",
        "prize":1,
        "available":10,
        "assigned":0
      }
  */


  // Comprobamos si quedan disponibles
  if (cheatMeta.assigned >= cheatMeta.available) {
    client.close();
    res.status(500).json({ message: "All cheat codes already redeemed" });
    return;
  }

  // Actualizamos el numero de cheat codes asignados
  await metaCollection.updateOne(
    { id: cheat_code },
    { $inc: { assigned: 1 } }
  );

  // Actualizamos el numero de creditos del usuario y el array de cheat codes del usuario
  await usersCollection.updateOne(
    { _id: userId },
    { $inc: { credits: cheatMeta.prize }, $push: { cheat_codes: cheat_code } }
  );

  // ================================================================
  // ================================================================
  client.close();

  res.status(200).json({ message: "Cheat code redeemed" });
}

export default CheatCodes;