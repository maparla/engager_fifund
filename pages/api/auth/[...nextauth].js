import NextAuth from "next-auth"
import { MongoDBAdapter } from "@next-auth/mongodb-adapter"

import DiscordProvider from "next-auth/providers/discord"

import clientPromise from "../../../lib/mongodb"
import { encrypt } from "../../../utils/crypt"

// For more information on each option (and a full list of options) go to
// https://next-auth.js.org/configuration/options
export default NextAuth({
    adapter: MongoDBAdapter(clientPromise),
    secret: process.env.SECRET,
    session: {
        jwt: true,
    },
    providers: [
        DiscordProvider({
            clientId: process.env.DISCORD_CLIENT_ID,
            clientSecret: process.env.DISCORD_CLIENT_SECRET,
        })
    ],
    pages: {
        signIn: "/", // Displays signin buttons
        signOut: "/", // Displays form with sign out button
        // error: "/auth/error", // Error code passed in query string as ?error=
        // verifyRequest: "/auth/verify-request", // Used for check email page
        // newUser: null, // If set, new users will be directed here on first signin
    },
    callbacks: {
        async jwt({ token }) {
            return token
        },
        async session({ session, token, user }) {
            // Send properties to the client, like an access_token from a provider.
            session.id = encrypt(user.id)
            session.name = user.name || ""
            session.avatar = user.image || ""

            // Filter the prizes with id card_bomb
            session.prizes = user.prizes || []

            // Get game history information
            let card_bomb_matches = user.game_history?.filter(game => game.id === "game_card_bomb") || []
            if (card_bomb_matches.length > 0) {
                // Create a dictionary with the sume of movements, how many results 'win' and 'lose'
                session.game_card_bomb_history = {
                    total_movements: 0,
                    total_wins: 0,
                    total_losses: 0
                }
                card_bomb_matches.forEach(match => {
                    session.game_card_bomb_history.total_movements += match.movements.length
                    session.game_card_bomb_history.total_wins += match.result === "win" ? 1 : 0
                    session.game_card_bomb_history.total_losses += match.result === "lose" ? 1 : 0
                })
            }else{
                session.game_card_bomb_history = {
                    total_movements: 0,
                    total_wins: 0,
                    total_losses: 0
                }
            }

            if (user.game_card_bomb){
                session.game_card_bomb_history.total_movements += user.game_card_bomb.movements.length
                session.game_card_bomb_history.total_wins += user.game_card_bomb.result === "win" ? 1 : 0
                session.game_card_bomb_history.total_losses += user.game_card_bomb.result === "lose" ? 1 : 0
            }

            session.credits = user.credits || 0
            session.pending_quests = user.pending_quests || []
            session.completed_quests = user.completed_quests || []

            // == SOCIALS

            session.twitter_name = user.socials?.twitter?.screen_name || ""
            session.twitter_id = user.socials?.twitter?.user_id || ""

            /* == GAMES SETUP == */
            session.game_card_bomb_movements = user.game_card_bomb?.movements || []
            session.game_card_bomb_result = user.game_card_bomb?.result || null

            return session
        }
    },
})