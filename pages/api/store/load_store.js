// Utilizando Atlas de https://cloud.mongodb.com/
import { MongoClient, ObjectId } from 'mongodb';
import { decrypt } from '../../../utils/crypt';

const DB_NAME = 'Games';
const USERS_COLLETION_NAME = "users";
const STORE_COLLETION_NAME = "store";

import { getSession } from "next-auth/react";


async function loadStore(req, res) {

  if (req.method !== "GET") {
    res.status(405).json("Method not allowed");
    return;
  }

  const session = await getSession({ req })
  if (!session) {
    res.status(401).json({ message: "User not signed in" });
    return
  }

  let userId = session.id;

  if (userId === undefined) {
    res.status(400).json("Malformed data");
    return;
  }

  userId = ObjectId(decrypt(userId));

  const client = await MongoClient.connect(
    `mongodb+srv://${process.env.MONGO_USERNAME}:${process.env.MONGO_PASSWORD}@${process.env.MONGO_CLUSTER}/${DB_NAME}?retryWrites=true&w=majority`
  );

  // Recordad que deberiamos comprobar si hay errores en alguno de los puntos
  const db = client.db();

  const usersCollection = db.collection(USERS_COLLETION_NAME);
  const user = await usersCollection.findOne({
    _id: userId
  });

  if (user === null) {
    client.close();
    res.status(404).json({ message: "User not found" });
    return;
  }

  // ================================================================
  // ================================================================

  // load the ALL store from the database
  const storeCollection = db.collection(STORE_COLLETION_NAME);
  const store = await storeCollection.find({}).toArray();

  // For each item in the store, we are going to add some fields to it

  // We are going to add 'total_entries' field to each item counting how many participations there are
  store.forEach(item => {
    // Sum up all item.participations.participations
    let total_entries = 0;
    // Take only the participations of the user
    let user_entries = 0;

    if (item.participations) {
      item.participations.forEach(participation => {
        total_entries += participation.participations;
        if (participation.user_id.equals(userId)) {
          user_entries += participation.participations;
        }
      });
    }

    item.total_entries = total_entries;
    item.user_entries = user_entries;

    // We must remove the participations field from each item, as can contain sensitive information
    delete item.participations;

    // Finally we can set if the user is a winner for the store item or not
    item.winner = false;
    if (item.winners) {
      for (let winner of item.winners) {
        if (winner.equals(userId)) {
          item.winner = true;
          break;
        }
      }
    } else if (item.type == "limited_edition") { // Handle the limited_edition cases
      // check if user item has prize_type at its prizes array
      if (user.prizes) {
        let hasPrize = user.prizes.find(prize => prize.type === item.prize_type);
        if (hasPrize) {
          item.winner = true;
        }
      }
    }
    delete item.winners;
  })


  // ================================================================
  // ================================================================

  client.close();

  res.status(200).json({ store });
}

export default loadStore;