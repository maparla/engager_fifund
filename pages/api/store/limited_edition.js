// Utilizando Atlas de https://cloud.mongodb.com/
import { MongoClient, ObjectId } from 'mongodb';
import { decrypt } from '../../../utils/crypt';

const DB_NAME = 'Games';
const USERS_COLLETION_NAME = "users";
const STORE_COLLETION_NAME = "store";

import { getSession } from "next-auth/react";


async function randomRaffleStore(req, res) {

  if (req.method !== "POST") {
    res.status(405).json("Method not allowed");
    return;
  }

  const session = await getSession({ req })
  if (!session) {
    res.status(401).json({ message: "User not signed in" });
    return
  }

  let userId = session.id;

  if (userId === undefined) {
    res.status(400).json("Malformed data");
    return;
  }

  userId = ObjectId(decrypt(userId));

  const client = await MongoClient.connect(
    `mongodb+srv://${process.env.MONGO_USERNAME}:${process.env.MONGO_PASSWORD}@${process.env.MONGO_CLUSTER}/${DB_NAME}?retryWrites=true&w=majority`
  );

  // Recordad que deberiamos comprobar si hay errores en alguno de los puntos
  const db = client.db();

  const usersCollection = db.collection(USERS_COLLETION_NAME);
  const user = await usersCollection.findOne({
    _id: userId
  });

  if (user === null) {
    client.close();
    res.status(404).json({ message: "User not found" });
    return;
  }

  // ================================================================
  // ================================================================

  let { item_id } = req.body;

  // ================================================================
  // ================================================================


  const storeCollection = db.collection(STORE_COLLETION_NAME);
  const storeItem = await storeCollection.findOne({
    _id: ObjectId(item_id)
  });


  // ================================================================
  // ================================================================

  // Check that the prizes > 0
  if (storeItem.prizes <= 0) {
    client.close();
    res.status(400).json("No prizes available");
    return;
  }

  // Check that the user has enough credits
  if (user.credits < storeItem.price) {
    client.close();
    res.status(400).json({ message: "Not enough credits" });
    return;
  }

  // Check that the item can be bought by status = true and end_date > now
  if (storeItem.status === false || new Date(storeItem.end_date) < new Date()) {
    client.close();
    res.status(400).json({ message: "Item not available" });
    return;
  }

  // Check the type is limited_edition
  if (storeItem.type !== "limited_edition") {
    client.close();
    res.status(400).json({ message: "Item not available" });
    return;
  }

  // ================================================================
  // ================================================================

  // Si esta todo bien, primero debemos restarle los creditos al usuario
  // y añadir el 'prize_type' al campo 'prizes' del usuario
  const newCredits = user.credits - storeItem.price;
  let prize = {
    "type": storeItem.prize_type,
    "status": "pending",
    "game": "limited_edition",
  }
  const newPrizes = user.prizes ? [...user.prizes, prize] : [prize];
  await usersCollection.updateOne(
    { _id: userId },
    {
      $set: {
        credits: newCredits,
        prizes: newPrizes
      }
    }
  );

  // Debemos añadir eliminar un prize del item y si no quedan, desactivarlo

  let newStatus = storeItem.prizes > 1 ? true : false;

  // Actualizamos el item
  await storeCollection.updateOne(
    { _id: ObjectId(item_id) },
    {
      $set: {
        prizes: storeItem.prizes - 1,
        status: newStatus
      }
    }
  );

  // ================================================================
  // ================================================================


  client.close();

  res.status(200).json({ message: "Item bought successfully" });
}

export default randomRaffleStore;