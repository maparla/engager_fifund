// Utilizando Atlas de https://cloud.mongodb.com/
import { MongoClient, ObjectId } from 'mongodb';
import { decrypt } from '../../../utils/crypt';

const DB_NAME = 'Games';
const USERS_COLLETION_NAME = "users";
const STORE_COLLETION_NAME = "store";

import { getSession } from "next-auth/react";


async function randomRaffleStore(req, res) {

  if (req.method !== "POST") {
    res.status(405).json("Method not allowed");
    return;
  }

  const session = await getSession({ req })
  if (!session) {
    res.status(401).json({ message: "User not signed in" });
    return
  }

  let userId = session.id;

  if (userId === undefined) {
    res.status(400).json("Malformed data");
    return;
  }

  userId = ObjectId(decrypt(userId));

  const client = await MongoClient.connect(
    `mongodb+srv://${process.env.MONGO_USERNAME}:${process.env.MONGO_PASSWORD}@${process.env.MONGO_CLUSTER}/${DB_NAME}?retryWrites=true&w=majority`
  );

  // Recordad que deberiamos comprobar si hay errores en alguno de los puntos
  const db = client.db();

  const usersCollection = db.collection(USERS_COLLETION_NAME);
  const user = await usersCollection.findOne({
    _id: userId
  });

  if (user === null) {
    client.close();
    res.status(404).json({ message: "User not found" });
    return;
  }

  // ================================================================
  // ================================================================

  let { item_id } = req.body;

  // ================================================================
  // ================================================================


  const storeCollection = db.collection(STORE_COLLETION_NAME);
  const storeItem = await storeCollection.findOne({
    _id: ObjectId(item_id)
  });


  // ================================================================
  // ================================================================

  // Check that the user has enough credits
  if (user.credits < storeItem.price) {
    client.close();
    res.status(400).json({ message: "Not enough credits" });
    return;
  }

  // Check that the item can be bought by status = true and end_date > now
  if (storeItem.status === false || new Date(storeItem.end_date) < new Date()) {
    client.close();
    res.status(400).json({ message: "Item not available" });
    return;
  }

  // Check the type is random_raffle
  if (storeItem.type !== "random_raffle") {
    client.close();
    res.status(400).json({ message: "Item not available" });
    return;
  }

  // ================================================================
  // ================================================================

  // Si esta todo bien, primero debemos restarle los creditos
  const newCredits = user.credits - storeItem.price;
  await usersCollection.findOneAndUpdate({
    _id: userId
  }, {
    $set: {
      credits: newCredits
    }
  });

  // Debemos añadir la participacion del usuario a la rifa,
  // teniendo en cuenta que podria tener de antes varias participaciones
  const userParticipations = storeItem.participations ? storeItem.participations.find(participation => {
    return participation.user_id.equals(userId);
  }) : [];

  if (userParticipations === undefined || userParticipations.length === 0) {
    if (storeItem.participations === undefined) {
      storeItem.participations = [];
    }
    storeItem.participations.push({
      user_id: userId,
      participations: 1
    });
  }
  else {
    userParticipations.participations++;
  }

  // Actualizamos la rifa
  await storeCollection.findOneAndUpdate({
    _id: ObjectId(item_id)
  }, {
    $set: {
      participations: storeItem.participations
    }
  });


  // ================================================================
  // ================================================================


  client.close();

  res.status(200).json({ message: "Enter logged successfully" });
}

export default randomRaffleStore;