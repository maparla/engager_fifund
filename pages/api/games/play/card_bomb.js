// Utilizando Atlas de https://cloud.mongodb.com/
import { MongoClient, ObjectId } from 'mongodb';
import { decrypt } from '../../../../utils/crypt';

const DB_NAME = 'Games';
const USERS_COLLETION_NAME = "users";
const META_COLLETION_NAME = "meta";

import { getSession } from "next-auth/react";

async function playGameCardBomb(req, res) {

    if (req.method !== "POST") {
        res.status(405).json("Method not allowed");
        return;
    }

    const session = await getSession({ req })
    if (!session) {
        res.status(401).json({ message: "User not signed in" });
        return
    }

    let userId = session.id;

    if (userId === undefined) {
        res.status(400).json("Malformed data");
        return;
    }

    userId = ObjectId(decrypt(userId));

    const client = await MongoClient.connect(
        `mongodb+srv://${process.env.MONGO_USERNAME}:${process.env.MONGO_PASSWORD}@${process.env.MONGO_CLUSTER}/${DB_NAME}?retryWrites=true&w=majority`
    );

    // Recordad que deberiamos comprobar si hay errores en alguno de los puntos
    const db = client.db();

    const usersCollection = db.collection(USERS_COLLETION_NAME);
    const user = await usersCollection.findOne({
        _id: userId
    });

    if (user === null) {
        client.close();
        res.status(404).json({ message: "User not found" });
        return;
    }

    let { card_indx } = req.body;

    // ================================================================
    // ================================================================

    let game_card_bomb_info;
    let current_credits = user.credits;
    
    // Si el usuario no tiene juegos activos, creamos uno nuevo
    if (!user.game_card_bomb) {

        // Debemos comprobar que el usuario dispone de suficientes créditos para jugar
        if (user.credits < 1) {
            client.close();
            res.status(400).json({ message: "Not enough credits" });
            return;
        }

        game_card_bomb_info = {
            bomb_index: Math.floor(Math.random() * (process.env.GAME_NUM_CARD_BOMB -1) ), // integer from 0 to GAME_NUM_CARD_BOMB -1
            date: new Date(),
            result: 'playing',
            movements: []
        }

        current_credits -= 1;

    // Si el usuario tiene juegos activos, actualizamos el juego actual
    } else if (user.game_card_bomb.result === 'win' || user.game_card_bomb.result === 'lose'){
        client.close();
        res.status(400).json("Please reset the game to play again");
        return;
    } else if (user.game_card_bomb.result === 'playing') {
        
        game_card_bomb_info = user.game_card_bomb;

        // Checkear que no se haya elegido una carta ya seleccionada
        if (game_card_bomb_info.movements.includes(card_indx)) {
            client.close();
            res.status(400).json({ message: "Card already played" });
            return;
        }

    } else {
        client.close();
        res.status(500).json({ message: "Unknown error" });
        return;
    }

    // Una vez tenemos el objeto del juego, debemos analizar la jugada del usuario
    game_card_bomb_info.movements.push(card_indx);

    let prizes = user.prizes? user.prizes : [];


    if (game_card_bomb_info.bomb_index === card_indx) {
        game_card_bomb_info.result = 'lose';
    
    // Si hemos dado la vuelta a todas las cartas menos una, ganamos
    } else if (game_card_bomb_info.movements.length === (process.env.GAME_NUM_CARD_BOMB -1) ) {
        game_card_bomb_info.result = 'win';


        // Asignamos un premio al usuario
        const metaCollection = db.collection(META_COLLETION_NAME);
        const gamesMeta = await metaCollection.findOne({
            id: "card_bomb"
        });

        if (gamesMeta === null) {
            client.close();
            res.status(500).json({ message: "Unknown error" });
            return;
        }

        /* gamesMeta has the following structure:
            {
                "_id": { "$oid": "62f26c94197c2f1a52b88397" },
                "id": "card_bomb",
                "prizes": [
                    { "type": "whitelist", "probability": 0.8, "available": 4000, "assigned": 0 },
                    { "type": "free_nft", "probability": 0.1, "available": 30, "assigned": 0 },
                    { "type": "01_sol", "probability": 0.07, "available": 15, "assigned": 0 },
                    { "type": "05_sol", "probability": 0.03, "available": 7, "assigned": 0 }
                ]
            }
        */

        // Select only prizes where assigned < available
        const available_prizes = gamesMeta.prizes.filter(prize => prize.assigned < prize.available);
       
        // Asign a random prize based on availability and probability
        if (available_prizes.length === 0) {
            prizes.push({ type: "empty", status: "pending", game: "card_bomb" });
        }else{

            // Select a random prize from the available prizes based on incremental probability
            // https://stackoverflow.com/a/69336827/6724947

            // Normalize the probabilities from 0 to 1
            let probs_sum = available_prizes.reduce((acc, cur) => acc + cur.probability, 0);
            //let probs_norm = available_prizes.map(prize => prize.probability / probs_sum);

            let sorted_prizes = available_prizes.sort((a, b) => {
                return a.probability - b.probability;
            });


            // Array of probabilities normalized, incrementally added
            let incremental_probabilities = [];
            incremental_probabilities.push(sorted_prizes[0].probability / probs_sum);
            for (let i = 1; i < sorted_prizes.length; i++) {
                incremental_probabilities.push(
                    sorted_prizes[i].probability / probs_sum + incremental_probabilities[i - 1]
                );
            }

            let random_number = Math.random();
            let selected_prize = sorted_prizes[0];
            for (let i = 0; i < incremental_probabilities.length; i++) {
                if (random_number < incremental_probabilities[i]) {
                    selected_prize = sorted_prizes[i];
                    break;
                }
            }

            prizes.push({ type: selected_prize.type, status: "pending", game: "card_bomb" });

            // With this, I must update the assigned count of the prize in the meta collection
            const prize_index = gamesMeta.prizes.findIndex(prize => prize.type === selected_prize.type);
            gamesMeta.prizes[prize_index].assigned += 1;
            await metaCollection.updateOne({ id: "card_bomb" }, { $set: { prizes: gamesMeta.prizes } });

        }
    
    } // Si no, seguimos jugando

    // Actualizamos el estado del juego y creditos en la base de datos
    await usersCollection.updateOne({
        _id: userId
    }, {
        $set: {
            game_card_bomb: game_card_bomb_info,
            credits: current_credits,
            prizes: prizes
        }
    });

    client.close();


    if (game_card_bomb_info.result === 'win') {
        res.status(201).json({ message: "You win!" });
    }
    else if (game_card_bomb_info.result === 'lose') {
        res.status(202).json({ message: "You lose :(" });
    }
    else {
        res.status(200).json({ message: "Nice move!" });
    }
}

export default playGameCardBomb;