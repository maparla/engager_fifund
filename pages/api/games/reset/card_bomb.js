// Utilizando Atlas de https://cloud.mongodb.com/
import { MongoClient, ObjectId } from 'mongodb';
import { decrypt } from '../../../../utils/crypt';

const DB_NAME = 'Games';
const USERS_COLLETION_NAME = "users";

import { getSession } from "next-auth/react";

async function resetGameCardBomb(req, res) {

    if (req.method !== "GET") {
        res.status(405).json("Method not allowed");
        return;
    }

    const session = await getSession({ req })
    if (!session) {
        res.status(401).json({ message: "User not signed in" });
        return
    }

    let userId = session.id;

    if (userId === undefined) {
        res.status(400).json("Malformed data");
        return;
    }

    userId = ObjectId(decrypt(userId));

    const client = await MongoClient.connect(
        `mongodb+srv://${process.env.MONGO_USERNAME}:${process.env.MONGO_PASSWORD}@${process.env.MONGO_CLUSTER}/${DB_NAME}?retryWrites=true&w=majority`
    );

    // Recordad que deberiamos comprobar si hay errores en alguno de los puntos
    const db = client.db();

    const usersCollection = db.collection(USERS_COLLETION_NAME);
    const user = await usersCollection.findOne({
        _id: userId
    });

    if (user === null) {
        client.close();
        res.status(404).json({ message: "User not found" });
        return;
    }

    // ================================================================
    // ================================================================

    
    
    if (!user.game_card_bomb){
        client.close();
        res.status(400).json("No game active");
        return;
    }
    
    // Si el usuario no tiene juegos finalizados, no hay nada que resetear...
    if (user.game_card_bomb.result !== 'win' && user.game_card_bomb.result !== 'lose') {
        client.close();
        res.status(400).json("No game finished");
        return;
    }

    let game_card_bomb_info = user.game_card_bomb;

    game_card_bomb_info.id = 'game_card_bomb';

    // Actualizamos el estado del juego, reseteandolo y añadiendolo a la historia
    await usersCollection.updateOne({
        _id: userId
    }, {
        $unset: {
            game_card_bomb: "",
        },
        $push: {
            game_history: game_card_bomb_info
        }
    });

    client.close();

    res.status(200).json({ message: "Game reset" });
}

export default resetGameCardBomb;