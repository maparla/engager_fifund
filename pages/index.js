import Head from 'next/head'
import Image from 'next/image'

import { useRouter } from 'next/router'

import { signIn, useSession } from "next-auth/react"

import styles from '../styles/Home.module.css'

export default function Home() {

  const router = useRouter()

  const { status } = useSession()
  const loading = status === "loading"

  if (loading) {
    return <div>Loading...</div>
  }

  if (status === "authenticated") {
    router.push('/quests');
    return (<></>);
  }

  return (
    <div className={styles.bg}>
      <div className={styles.container}>
        <h1 className={styles.login_title}>
          <span>Booster</span>
          <span>by FiFund</span>
        </h1>
        <button
          className={styles.login_button}
          onClick={async () => {
            await signIn("discord", { redirect: false, callbackUrl: "/quests" });
          }}>
          Log in with Discord
        </button>

      </div>
    </div>
  )
}
