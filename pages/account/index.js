import { useSession } from "next-auth/react"

import styles from './Account.module.css'
import { Layout } from '../../components/Layout'
import ImageWithFallback from "../../components/ImageWithFallback/ImageWithFallback"

import Link from "next/link"
import { GAME_TO_ICON, GAME_TO_IMAGE, PRIZES_TO_NAMES, PRIZE_TO_IMAGE } from "../../utils/prizes"
import Image from "next/image"
import useTwitterLogin from "../../hooks/useTwitterLogin"

import { FiTwitter, FiTrash2, FiCheck } from "react-icons/fi";

import { shortWalletString } from "../../utils/wallets"
import { useState } from "react"
import useQuests from "../../hooks/useQuests"
import { Loader } from "../../components/Loader/Loader"
import { toast } from "react-toastify"


const capitalize = (s) => s.charAt(0).toUpperCase() + s.slice(1);


export default function Account() {

    const { data: session } = useSession();
    const [cheatCode, setCheatCode] = useState("");

    const {
        perform: performingQuests,
        performQuest,
    } = useQuests();

    const {
        loading: loadingTwitter,
        unlinkAction: unlinkTwitter,
        registerIntention: registerTwitterLinkIntention,
    } = useTwitterLogin();

    // Filter the prizes with type different than nothing
    let actual_prizes = session?.prizes?.filter(prize => prize.type !== "nothing") || [];

    return (
        <Layout>
            <div className={styles.container}>
                <div className={styles.header}>
                    <h1>Account</h1>
                    <ImageWithFallback
                        src={session?.avatar}
                        width={50}
                        height={50}
                        alt={`${session?.name} Discord Picture`}
                        fallbackSrc={`/images/micah.svg`}
                        placeholder={`/images/micah.svg`}
                        className={styles.avatar}
                    />
                </div>

                <div className={styles.description}>
                    <p>Hi {session?.name}! Here you will find all your account details and settings.</p>
                </div>

                <div className={`${styles.cheat_code_section} ${styles.section}`}>
                    <h2>Cheat Codes</h2>
                    <p className={styles.description}>Do you have a cheat code? Enter it here to get a prize!</p>
                    <div className={styles.cheat_code_input}>
                        <input
                            type="text"
                            placeholder="Enter your cheat codes here"
                            value={cheatCode}
                            onChange={(e) => setCheatCode(e.target.value)}
                        />
                        <button
                            onClick={() => {
                                if (cheatCode) {
                                    performQuest(
                                        "cheat_code",
                                        session?.twitter_id ? true : false,
                                        cheatCode
                                    );
                                    setCheatCode("");
                                }else{
                                    toast.error("Please enter a cheat code");
                                }
                            }}
                        >
                            <FiCheck />
                        </button>
                    </div>
                </div>


                <div className={`${styles.socials} ${styles.section}`}>
                    <h2>Socials</h2>
                    <p className={styles.description}>
                        You need to link some socials accounts to be able to complete some quests.
                    </p>

                    <div className={styles.socials_accounts}>
                        {!session?.twitter_name && (
                            <div
                                className={`${styles.social} ${styles.link_twitter}`}
                                onClick={registerTwitterLinkIntention}
                            >
                                <FiTwitter /> Link Twitter
                            </div>
                        )}
                    </div>

                    <div className={styles.prizes_list}>

                        {/* Link Twitter */}
                        {session?.twitter_name && (
                            <div className={styles.prize}>

                                <div className={styles.prize_image_container}>
                                    <Image
                                        src='/images/account/socials/twitter_banner.jpg'
                                        layout="fill"
                                        objectFit="cover"
                                        alt={`Twitter banner`}
                                        className={styles.prize_image}
                                    />
                                </div>

                                <div className={styles.prize_meta}>
                                    <div className={`${styles.prize_info}`}>
                                        <p>Account: <span className={styles.prize_val}>@{session?.twitter_name}</span></p>
                                        <p>User ID: <span className={styles.prize_val}>{session?.twitter_id ? shortWalletString(session.twitter_id) : ''}</span></p>
                                    </div>
                                    <div
                                        className={styles.prize_type}
                                        onClick={unlinkTwitter}
                                        style={{ cursor: "pointer" }}
                                    >
                                        <FiTrash2 />
                                    </div>
                                </div>

                            </div>
                        )}

                    </div>


                </div>

                <div className={`${styles.prizes} ${styles.section}`}>
                    <h2>Prizes</h2>
                    <div className={styles.description}>
                        {actual_prizes.length === 0 &&
                            <>
                                <p>You have no prizes.</p>
                                {session?.credits === 0 ?
                                    <p>Earn more credits at <Link href='/quests'><b style={{ cursor: 'pointer', textDecorationLine: 'underline' }} >Quests section</b></Link>.</p>
                                    :
                                    <p>Try your luck <Link href='/play'><b style={{ cursor: 'pointer', textDecorationLine: 'underline' }} >playing a game</b></Link>.</p>
                                }
                            </>
                        }{
                            actual_prizes.length > 0 &&
                            <>
                                <p>You have a total of {actual_prizes.length} prize{actual_prizes.length > 1 ? 's' : ''}. Please send a screenshot to the quests-proofs channel in our discord to claim your prize{actual_prizes.length > 1 ? 's' : ''}.</p>
                                <div className={styles.prizes_list}>
                                    {actual_prizes.map((prize, index) => (
                                        <div key={index} className={styles.prize}>
                                            <div className={styles.prize_image_container}>
                                                <Image
                                                    src={PRIZE_TO_IMAGE[prize.type]}
                                                    layout="fill"
                                                    objectFit="cover"
                                                    alt={`${prize.type} banner`}
                                                    className={styles.prize_image}
                                                />
                                            </div>
                                            <div className={styles.prize_meta}>
                                                <div className={styles.prize_info}>
                                                    <p>Prize: <span className={styles.prize_val}>{capitalize(PRIZES_TO_NAMES[prize.type])}</span></p>
                                                    <p>Status: <span className={styles.prize_val}>{capitalize(prize.status)}</span></p>
                                                </div>
                                                <div className={styles.prize_type}>
                                                    {GAME_TO_ICON[prize.game]}
                                                </div>
                                            </div>
                                        </div>
                                    ))}
                                </div>
                            </>
                        }
                    </div>
                </div>

                <div className={`${styles.matches} ${styles.section}`}>
                    <h2>Matches</h2>
                    <p className={styles.description}>
                        Here you can find the stats of your matches.
                    </p>

                    <div className={styles.prizes_list}>

                        <Link href='/play'>
                            {/* Game card bomb */}
                            <div className={styles.prize} style={{ cursor: 'pointer' }}>

                                <div className={styles.prize_image_container}>
                                    <Image
                                        src={GAME_TO_IMAGE["game_card_bomb"]}
                                        layout="fill"
                                        objectFit="cover"
                                        alt={`Game card bomb banner`}
                                        className={styles.prize_image}
                                    />
                                </div>
                                <div className={styles.prize_meta}>
                                    <div className={styles.prize_info}>
                                        <p>{session?.game_card_bomb_history?.total_movements} cards revealed!</p>
                                        <p>{session?.game_card_bomb_history?.total_wins} wins / {session?.game_card_bomb_history?.total_losses} losses</p>
                                    </div>
                                </div>

                            </div>
                        </Link>

                    </div>

                </div>

                {performingQuests && (
                    <div className={styles.loader_container}>
                        <Loader scale={0.25} transition_time={0.5} />
                    </div>
                )}

            </div>
        </Layout>
    )
}
