## Launch

```
yarn install
npm run build
pm2 start npm --name "booster_fifund" -- start
```

## Minimum bots

- follow fifund twitter
- store random raffle

## Store items can be

- random_raffle: Rifa aleatoria
- limited_edition: Quien lo compra antes, antes lo tiene

- status:
   - false: No disponible
   - true: Disponible

Una vez añadido un premio, debemos añadir su info en el archivo `utils/prizes.js`

## DB Structure

Store items should be stored in a table with the following structure:

```
{
   "_id":{
      "$oid":"62f9f3a78d5ef1cd01f6ab69"
   },
   "end_date":{
      "$date":"2022-08-17T06:00:00.000Z"
   },
   "price":2,
   "type":"random_raffle",
   "participations":[
      {
         "user_id":{
            "$oid":"62f09df5a5dd48b7c8770c12"
         },
         "participations":5
      }  
   ],
   "image":"1855-dead.png",
   "status":false,
   "prizes":1,
   "description":"DeGod #1855 NFT",
   "name":"DeGod NFT",
   "multi_win":true,
   "prize_type":"nft_degod_1855",
   "winners":[
      {
         "$oid":"62f09df5a5dd48b7c8770c12"
      }
   ]
}
```

or:

```
{
   "_id":{
      "$oid":"63046c7f86d75db729683931"
   },
   "end_date":{
      "$date":"2022-08-25T06:00:00.000Z"
   },
   "price":2,
   "type":"limited_edition",
   "image":"whitelist.png",
   "status":false,
   "prizes":0,
   "description":"FiFund Whitelist Access",
   "name":"FiFund Whitelist",
   "multi_win":false,
   "prize_type":"fifund_whitelist_v0"
}
```

## To-Do

- [x] Bot `random_raffle`.
- [x] Mostrar status store en Account.
- [x] Display winner indicator + open ticket.
- [x] Quest Twitter
- [-] Estilos responsive
- [x] Limited Edition Store Item
- [ ] Credits prize
- [ ] Documentate all
- [ ] Logica de `unique_number`. Load, display info y bot.
- [ ] Load quests from database (ease the credits management).
- [ ] Mas Quests
